<?php

$path = "test.sqlite";

if (file_exists($path)) {
    $db = new SQLite3($path);

    $result = $db->query("SELECT email FROM listing");

    while ($data = $result->fetchArray()) {

        $item = explode('@',$data['email']);

		$data['alias'] = $item[0];
		$data['domain'] = $item[1];

        $db->query("INSERT INTO address (alias,domain,state) VALUES ('{$data['alias']}','{$data['domain']}','none')");

        echo $data['email']."\n";

    }

    $db->query("DELETE FROM listing");

} else {
    echo "Missing database !";
}
