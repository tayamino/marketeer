<?php

require_once("vendor/autoload.php");

use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\SMTP;

//##########################################################################################

function is_email ($input) {
    $email_pattern = "/^([a-zA-Z0-9\-\_\.]{1,})+@+([a-zA-Z0-9\-\_\.]{1,})+\.+([a-z]{2,4})$/i";
    if(preg_match($email_pattern, $input)) return TRUE;
}

/******************************************************************************/

function is_phone ($input) {
    return true;
    $phone_pattern = "/^([a-zA-Z0-9\-\_\.]{1,})+@+([a-zA-Z0-9\-\_\.]{1,})+\.+([a-z]{2,4})$/i";
    if(preg_match($phone_pattern, $input)) return TRUE;
}

//##########################################################################################

function randString($consonants, $min_length, $max_length) {
    $length=rand($min_length, $max_length);
    $password = "";
    for ($i = 0; $i < $length; $i++) {
            $password .= $consonants[(rand() % strlen($consonants))];
    }
    return $password;
}

/******************************************************************************/

function formats($text,$data) {
    return $text;

    $listing = [
        "[-today-]" => date("m/d/Y", time()),
        "[-now-]" => date("h:i:s a", time()),
        "[-email-]" => $data['email'],
        "[-title-]" => $data['title'],
        //"[-link-]" => $data['link'],
        "[-letters-]" => randString("abcdefghijklmnopqrstuvwxyz", 8, 15),
        "[-string-]" => randString("abcdefghijklmnopqrstuvwxyz0123456789", 8, 15),
        "[-number-]" => randString("0123456789", 7, 15),
        "[-md5-]" => md5(rand()),
    ];

    foreach ($listing as $search => $value) {
        $text = str_replace($search, $value, $text);
    }

    $text = str_replace("\n", "", $text);

    return $text;
}

//##########################################################################################

function request ($url,$body,$head,$auth) {
    $resp = [];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");

    curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    if ($auth!=":" && $auth!=null)
        curl_setopt($ch, CURLOPT_USERPWD, $auth);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $resp["error"] = curl_error($ch);
    }
    curl_close($ch);

    $resp["result"] = json_decode($result,true);

    return $resp;
}

//##########################################################################################

if ($_SERVER['REQUEST_METHOD']=='POST') {
    $data = json_decode(file_get_contents("php://input"),true);
    
    //var_dump($data);die(1);

    $resp = [
        'form' => $data,
        'text' => "",
    ];

    try {
        //Create an instance; passing `true` enables exceptions
        $mail = new PHPMailer(true);

        $mail->Debugoutput = function($str, $level) {
            $GLOBALS['resp']['text'] .= "$level: $str\n";
        };

        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = $data['server']['host'];                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = $data['server']['username'];                     //SMTP username
        $mail->Password   = $data['server']['password'];                               //SMTP password
        $mail->Port       = $data['server']['port'];                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        switch ($data['server']['type']) {
            case 'ssl':
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                break;
            case 'tls':
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
                break;
            default:
                //$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                break;
        }

        $mail->setFrom($data['server']['from_email']);

        $mail->addAddress($data['address']);

        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = $data['subject'];
        $mail->Body    = $data['content'];
        $mail->AltBody = $data['message'];

        $mail->send();

        $resp['code'] = 'success';
        //echo 'Message has been sent';
    } catch (Exception $e) {
        $resp['code'] = 'error';

        $resp['text'] = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

    header('Content-Type: application/json');
    echo json_encode($resp);
} else {
?><!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="generator" content="">
    <title>Many'n'Mass</title>

    <!-- Bootstrap core CSS -->
    <link href="/asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="/asset/css/font-awesome.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .clignote
{
  animation: Test 1s infinite;
  color:blue;
}
.success
{
  color:green;
  font-weight: bold
}
.error
{
  color:red;
  font-weight: bold
}
.devider
{
 height: 20px;
 width: 100%;
}
.normal
{
  color:black;

}
@keyframes Test{
    0%{opacity: 1;}
    50%{opacity: 0;}
    100%{opacity: 1;}
}
div#progress {
    border: solid 1px gray;
}
label[for="multi"],input#multi{
    width: 25%;
    float: left;
}
label[for="limit"],input#limit{
    width: 25%;
    float: left;
}
label[for="sleep"],input#sleep{
    width: 25%;
    float: right;
}
label[for="await"],input#await{
    width: 25%;
    float: right;
}
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
#evolution {
    overflow-x: hidden;
    overflow-y: scroll;
    height: 220px;
}
    </style>
    <link href="/asset/css/dashboard.css" rel="stylesheet">
    <link href="/asset/custom.css" rel="stylesheet">
  </head>
  <body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">Many'n'Mass</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="#">Sign out</a>
    </li>
  </ul>
</header>

<div class="container-fluid">
  <div class="row">
    <main class="col-md-12 ms-sm-auto px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Many'n'Mass</h1>
        <div class="col-md-3">
            <label for="limit">Cycle</label>
            <label for="multi">Parallel</label>
            <label for="sleep">Size</label>
            <label for="await">Timeout</label>
            <input type="text" id="limit" class="form-control" placeholder="Cycle" value="" readonly>
            <input type="text" id="multi" class="form-control" placeholder="Multi" value="4">
            <input type="text" id="sleep" class="form-control" placeholder="Size" value="" readonly>
            <input type="text" id="await" class="form-control" placeholder="Timeout" value="60">
        </div>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button id="tool_start" type="button" class="btn btn-sm btn-success">
                <i class="fa fa-play"></i>
                Start
            </button>
            <button id="tool_pause" type="button" class="btn btn-sm btn-warning" disabled>
                <i class="fa fa-pause"></i>
                Pause
            </button>
            <button id="tool_stop" type="button" class="btn btn-sm btn-danger" disabled>
                <i class="fa fa-stop"></i>
                Stop
            </button>
            <button id="tool_backup" type="button" class="btn btn-sm btn-primary">
                <i class="fa fa-save"></i>
                Backup
            </button>
            <button id="tool_mockup" type="button" class="btn btn-sm btn-light">
                <i class="fa fa-refresh"></i>
                Restore
            </button>
            <a target="_window" href="sending.html" class="btn btn-sm btn-dark">
                <i class="fa fa-code-fork"></i>
                Fork
            </a>
          </div>
        </div>
      </div>
<div class="row">
    <form id="smtp_config" class="col-md-6">
        <ul id="servers" class="nav nav-tabs" role="tablist">
            <li class="nav-item" role="presentation">
                <a href="#" class="nav-link add-server" data-toggle="tab">
                    <i class="fa fa-plus"></i>
                </a>
            </li>
        </ul>
        <div id="serversContent" class="tab-content">
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="text" class="form-control col-10" placeholder="From Name" id="from_name" value="Full name">
            </div>
        </div>
        <hr>
        <input type="text" class="form-control col-10" placeholder="Subject" id="subject" value="test | IP:127.0.0.1">
        <hr>
        <div class="progress">
          <div id="progress" class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <hr>
        <div id="evolution" class="form-control col-12"></div>
    </form>
    <div class="col-md-6">
        <ul id="targets" class="nav nav-tabs" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="listing-tab" data-bs-toggle="tab" data-bs-target="#listing" type="button" role="tab" aria-controls="listing" aria-selected="true">
                    Contacts
                </button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="message-tab" data-bs-toggle="tab" data-bs-target="#message" type="button" role="tab" aria-controls="message" aria-selected="false">
                    <span class="visible-xs">Text</span>
                    <span class="hidden-xs">Message</span>
                </button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="content-tab" data-bs-toggle="tab" data-bs-target="#content" type="button" role="tab" aria-controls="content" aria-selected="false">
                    <span class="visible-xs">HTML</span>
                    <span class="hidden-xs">Content</span>
                </button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="debuger-tab" data-bs-toggle="tab" data-bs-target="#debuger" type="button" role="tab" aria-controls="debuger" aria-selected="false">
                    Debuging
                </button>
            </li>
        </ul>
        <div id="targetsContent" class="tab-content">
            <div id="listing" class="tab-pane fade show active" role="tabpanel" aria-labelledby="listing-tab">
                <textarea id="contact" rows="18" class="form-control" placeholder="Emails"></textarea>
            </div>
            <div id="message" class="tab-pane fade" role="tabpanel" aria-labelledby="message-tab">
                <textarea id="content_text" rows="18" class="form-control" placeholder="Content (text)"></textarea>
            </div>
            <div id="content" class="tab-pane fade" role="tabpanel" aria-labelledby="content-tab">
                <textarea id="content_html" rows="18" class="form-control" placeholder="Content (html)"></textarea>
            </div>
            <div id="debuger" class="tab-pane fade" role="tabpanel" aria-labelledby="debuger-tab">
                <textarea id="logging" rows="18" class="form-control" placeholder="Debug Output" readonly></textarea>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div id="errorlog" class="col-lg-16"></div>
    </div>
</div>
    </main>
  </div>
</div>
    <script id="server_form" type="text/html">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <input type="text" id="server_{{ index }}_username" name="username[]" class="form-control" placeholder="Username" value="{{ username }}">
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <input type="password" id="server_{{ index }}_password" name="password[]" class="form-control" placeholder="Password" value="{{ password }}">
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-6">
        <input type="text" id="server_{{ index }}_host" name="host[]" class="form-control" placeholder="Hostname" value="{{ host }}">
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3">
        <input type="number" id="server_{{ index }}_port" name="port[]" class="form-control" placeholder="Port" value="{{ port }}">
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3">
        <select id="server_{{ index }}_type" name="type[]" class="form-control col-12">
            <option value="unset">Default</option>
            <option value="none">Plain</option>
            <option value="ssl">SSL</option>
            <option value="tls" selected>TLS</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <input type="text" id="server_{{ index }}_from_email" name="from_email[]" class="form-control col-10" placeholder="From Email" value="{{ from_email }}">
    </div>
    <div class="col-md-3">
        <select id="server_{{ index }}_demo" name="template[]" class="form-control col-12" onchange="config_smtp(this);">
            <option value="test">Template</option>
            <option
                data-host="localhost"
                data-port="25"
                data-type="plain"
            value="local">Local</option>
        </select>
    </div>
    <div class="col-md-3">
        <input type="number" id="server_{{ index }}_limit" name="limit[]" class="form-control col-10" placeholder="Send Limit" value="{{ limit }}">
    </div>
</div>
    </script>
    <script src="/asset/js/jquery-3.6.0.min.js"></script>
        <script src="/asset/js/bootstrap.bundle.min.js"></script>
        <script src="/asset/js/twig.js"></script>
        <script src="/asset/js/Chart.min.js"></script>
    <script src="/asset/js/jquery.treed.js"></script>
        <script src="/asset/js/d3-collection.v1.min.js"></script>
        <script src="/asset/js/d3-dispatch.v1.min.js"></script>
        <script src="/asset/js/d3-dsv.v1.min.js"></script>
        <script src="/asset/js/d3-request.v1.min.js"></script>
        <script src="/asset/js/d3-queue.v3.min.js"></script>
    <script src="/asset/custom.js"></script>
    <script src="/asset/module.js"></script>
    <script src="/asset/handle.js"></script>
  </body>
</html>
<?php
}

