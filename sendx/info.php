<?php

$path = "test.sqlite";

$api_key = "c0crgbci3r75kwwczl044nobv0z2g47k";

if (file_exists($path)) {
    $db = new SQLite3($path);

    $result = $db->query("SELECT email FROM listing");
	
	$i = 0;

    while ($data = $result->fetchArray()) {

        $item = explode('@',$data['email']);
		
		$data['alias'] = $item[0];
		$data['domain'] = $item[1];
		
		$path = "mail/{$data['email']}.json";
		
		if (file_exists($path)) {
			$info = file_get_contents($path);
		} else {
			$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'https://emailcrawlr.com/api/v2/email?email='.$data['email']);
				curl_setopt($ch, CURLOPT_HTTPHEADER, [
					'x-api-key' => $api_key,
				]);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
			$info = curl_exec($ch);

            if (strlen($info)!=0) {
				file_put_contents($path,$info);
			}

			curl_close($ch);
		}
		
		$info = json_decode($info,false);

        $db->query("INSERT INTO address (alias,domain,state) VALUES ('{$data['alias']}','{$data['domain']}','none')");

		echo "{$i} {$data['email']}\n";
		
		$i = $i + 1;

    }

    $db->query("DELETE FROM listing");

} else {
    echo "Missing database !";
}
