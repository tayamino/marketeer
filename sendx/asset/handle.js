function config_smtp (obj) {
    var opt = obj.options[obj.options.selectedIndex];

    update_smtp(opt,'host');
    update_smtp(opt,'port');
    update_smtp(opt,'type');
}
function update_smtp (opt,key) {
    var obj = opt.attributes.getNamedItem('data-'+key);

    if (obj!=null) {
        document.getElementById(key).value = obj.value;
    }
}

Manager.checks = function (entry) {
    return emailReg.test(entry);
};

Manager.params = function () {
    if (
        !highlight('host') || !highlight('port') || !highlight('type')
    ||
        !highlight('username') || !highlight('password') || !highlight('contact')
    ||
        !highlight('from_mail') || !highlight('from_name') || !highlight('subject')
    ||
        !(highlight('content_text') || highlight('content_html'))
    ) {
        return false;
    }
    return true;
};

Manager.dumber = function () {
    var demo = 10;
    var zone = document.getElementById('contact');

    for (var i=0 ; i<demo ; i++) {
        zone.value += "mailbox_"+i+"@example.org\n";
    }

    document.getElementById('host').value = "mail.example.org";
    document.getElementById('port').value = "587";

    document.getElementById('username').value = "sender@example.org";
    document.getElementById('password').value = "password";

    document.getElementById('subject').value = "sender@example.org";
    document.getElementById('subject').value = "test | IP:127.0.0.1";

    document.getElementById('content_text').value = "k1 : v1\nk2 : v2\nk3 : v3";
    document.getElementById('content_html').value = "";
};

Manager.listed = function (index,value) {
    var router=document.getElementById("type").value;

    queue.defer(function (data, callback) {
        d3.request("?module=smtp"+((debug)?"&debug":""))
              .post(JSON.stringify(data), function (error, result) {
                  return Manager.result(error, result, callback);
              });
    },{
        host: document.getElementById("host").value,
        port: document.getElementById("port").value,
        type: document.getElementById("type").value,

        username: document.getElementById("username").value,
        password: document.getElementById("password").value,

        from_mail:document.getElementById("from_mail").value,
        from_name:document.getElementById("from_name").value,
        subject:document.getElementById("subject").value,

        index:index,
        total:procs.length,
        email:value,
        message:document.getElementById('content_text').value,
        content:document.getElementById('content_html').value,
        router:router,
    });
};
Manager.result = function (error, result, callback) {
    var response = null;

    try {
        response = JSON.parse(result.response);
    } catch (err) {
        response = {
            error : err,
            echo  : result.response,
        };
    }

    jQuery("#logging").val(response.echo);

    error = error || response.error;

    jQuery("#progress").append('<div class="col-lg-3">' + (response.index+1).toString() + '/' + response.total + '</div><div class="col-lg-6">' + response.email + '</div>');

    if (error){
        jQuery("#errorlog").append('<p>'+JSON.stringify(error)+'</p>');

        jQuery("#progress").append('<div class="col-lg-1"><span class="label label-danger">failed</span></div>');
    } else {
        jQuery("#progress").append('<div class="col-lg-1"><span class="label label-success">success</span></div>');
    }

    jQuery("#progress").append('<br>');

    window.scrollTo(0,document.body.scrollHeight);

    callback(error, response);
};
