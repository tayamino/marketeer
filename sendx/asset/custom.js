var Endpoint = {
    plug: function (key,url,app,sec) {
        Endpoint.grid[key] = new Datalake(key,url,app,sec);
        
        return Endpoint.grid[key];
    },
    grid: {},
    view: function (source,context) {
        var template = Twig.twig({
            data: $(source).html(),
        });

        resp = template.render(context);
        
        return resp;
    },
    load: function (source,target,callback,behavior) {
        var tpl = $(source).html();

        if (callback) {
            tpl = callback(tpl);
        }

        switch (behavior) {
            case 'prepend':
                $(target).prepend(tpl);
                break;
            case 'append':
                $(target).append(tpl);
                break;
            default:
                $(target).html(tpl);
                break;
        }
    },
    ajax: function (acte,args,hook,fail,encode) {
        var link = localStorage['endpoint'];

        $.ajax({
            url: Endpoint.conn.link+"/?acte="+acte+"&"+args,
            dataType: encode || "json",
            success: hook,
            error: fail,
        });
    },
    name: function (alias,value) {
        if (!(Endpoint.vars[alias])) {
            Endpoint.vars[alias] = value;
        }
        return Endpoint.vars[alias];
    },
    init: function (alias,value) {
        if (!(Endpoint.args[alias])) {
            Endpoint.args[alias] = value;
        }
        return Endpoint.args[alias];
    },
    vars: {
    },
    args: {},
    conn: null
};

Endpoint.conn = JSON.parse(localStorage['endpoint'] || '{}');

if (!(Endpoint.conn.host)) Endpoint.conn.host = 'localhost';
if (!(Endpoint.conn.port)) Endpoint.conn.port = 3000;
if (!(Endpoint.conn.type)) Endpoint.conn.type = 'http';

Endpoint.conn.link = Endpoint.conn.type+'://'+Endpoint.conn.host+':'+Endpoint.conn.port;

jQuery(function ($) {
    var qs = window.location.search.substring(1).split('&');

    for (i=0 ; i<qs.length ; i++) {
        pair = qs[i].split('=');
        
        Endpoint.args[pair[0]] = pair[1];
    }

    $('header input').keypress(function (ev) {
        if (ev.originalEvent.charCode==13) {
            var word = $(ev.currentTarget).val();
            var link = word.split(' ').join('+');

            //console.log('Searching "'+word+'": ',"/finding.html?q="+link);

            window.location.replace("/finding.html?q="+link);
        }
    });
    
    $('.dropdown-toggle').each(function (idx,obj) {
        $(obj).dropdown();
    });
});

function replace_all (origin,source,target) {
    var result = origin;
    var number = 0;
    
    while (result.search(source) && number<10) {
        result = result.replace(source,target); number += 1;
    }
    
    return result;
}

function highlight(name) {
    var obj = document.getElementById(name);

    if (obj.value.trim().length==0) {
        alert("Empty field : "+name);

        obj.focus();

        return false;
    }

    return true;
}

