<?php

//require_once ('../vendor/autoload.php');

use JeroenDesloovere\VCard\VCardParser;

//##############################################################################

include_once ('../engin/php/helpers.php');
include_once ('../engin/php/library.php');
include_once ('../engin/php/formats.php');

/******************************************************************************/

foreach ([
    'backend',
    'contact',
    'filesystem',
    'indexing',
    'mockups',
    'networks',
    'protocol',
    'shopping',
    'transport',
    'verificate',
] as $key) {
    require_once("../engin/uni/{$key}.php");
}

//##############################################################################

if (array_key_exists($_GET['acte'],$actes)) {
    $info = $actes[$_GET['acte']];
    
    $func = $info['func'];
    $data = [];
    
    $resp = $func();
    
    switch ($info['type']) {
        case 'json':
            header('Content-Type: application/json');
            echo json_encode($resp);
            break;
        case 'html':
            echo $resp;
            break;
    }
} else {
    echo "Invalid command : {$_GET['acte']}\n\n\n";
    echo print_r($_SERVER);
}

