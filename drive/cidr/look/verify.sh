#!/bin/bash

if [[ ! -f origin.txt ]] ; then
    wget -q "https://ftp.ripe.net/ripe/asnames/asn.txt" -O - > origin.txt
fi

if [[ ! -f source.txt ]] ; then
    echo "No 'source.txt' file provided !"
    
    exit
fi

if [[ ! -f lookup.txt ]] ; then
    for asn in $(cat origin.txt | grep -e Kaspersky -e Avast -e AVG -e ",MA" | awk '{print $1}') ; do
        whois -h whois.radb.net -- '-i origin AS'$asn | grep -Eo "([0-9.]+){4}/[0-9]+" >>lookup.txt
    done
    
    cat lookup.txt | sort | uniq | tee lookup.txt 2>&1 1>/dev/null
fi

#for address in $(cat source.txt) ; do
#    for network in $(cat target.txt) ; do
#        echo "Checking $address in $network"
#    done
#done

grepcidr -f lookup.txt source.txt | tee target.txt

