#!/usr/bin/env python3

import subprocess
 
def asn_lookup(index,title):
    run = subprocess.run([
        'bash','enumer.sh','AS' + index
    ], stdout=subprocess.PIPE)

    for line in run.stdout.decode("utf-8").split('\n'):
        if len(line):
            yield line

from tqdm import tqdm

def read_it(path):
    return open(path).read().split("\n")

word = read_it('key.txt')

with open("lst.txt","w+") as f:
    for line in tqdm(read_it('asn.txt')):
        line = line.split(' ',1)
        
        flag = False
        
        for key in word:
            if key.lower() in line[1].lower():
                flag = True

        if flag:
            data = asn_lookup(line[0],line[1])
            
            for item in tqdm(data,leave=False):
                f.write("%s,%s\n" % (line[0],item))

print("All done.")

