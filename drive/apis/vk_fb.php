<?php
function showOAuthError($error, $error_description){
    if($error == 'access_denied'){
        header("Location: http://".$_SERVER['HTTP_HOST']);
    }else{
        $error = '<strong>Error: '.$error.'</strong>
              <p>'.preg_replace('/\+/', ' ', $error_description).'</p>';

        die($error);
    };
}

function getRequestString($url, $params){
    $parsed_params = '?';

    foreach($params as $key => $value){
        $parsed_params .= $key.'='.$value.'&';
    };

    $parsed_params = trim($parsed_params, '&');

    if($params !== null){
        return $url.$parsed_params;
    }else{
        return $url;
    };
}

function doGetRequest($section, $params){
    header("Location: ".$this->getRequestString($section, $params));
}

function readGetRequest($section, $params, $json = true){
    $result = file_get_contents($this->getRequestString($section, $params));
    
    if($json){
        return json_decode($result);
    }else{
        return $result;
    };
}

function oauthVK(){
    $oauth_client_id      = '2899938';
    $oauth_secure_key     = 'IX61G9joOfg8rG0F5tWb';
    $oauth_scope          = '';

    if(isset($_GET['error'])){
        $this->showOAuthError($_GET['error'], $_GET['error_description']);
        
    }else{
        switch($_GET['step']){
            case 'auth' : {
                $this->doGetRequest('https://api.vkontakte.ru/oauth/authorize', array(
                    'client_id'     => $oauth_client_id,
                    'scope'         => $oauth_scope,
                    'response_type' => 'code',
                    'redirect_uri'  => urlencode('http://'.$_SERVER['HTTP_HOST'].'/control/auth/login?oauth&provider=vk&step=receive_code')
                ));

            }; break;

            case 'receive_code' : {
                $response = $this->readGetRequest('https://api.vkontakte.ru/oauth/access_token', array(
                    'client_id'     => $oauth_client_id,
                    'client_secret' => $oauth_secure_key,
                    'code'          => $_GET['code']
                ));

                if($response->error){
                    $this->showOAuthError($response->error, $response->error_description);
                }else{
                    $response = $this->readGetRequest('https://api.vkontakte.ru/method/getProfiles', array(
                        'fields'        => 'uid,first_name,last_name,nickname,domain,sex,bdate,city,country,timezone,photo,photo_medium,photo_big,has_mobile,contacts',
                        'uid'           => $response->user_id,
                        'access_token'  => $response->access_token
                    ));

                    $data = $response->response[0];

                    $this->registerFromOAuth('vk', array(
                        'uid'               => $data->uid,
                        'first_name'        => $data->first_name,
                        'last_name'         => $data->last_name,
                        'name'              => $data->nickname,
                        'login'             => $data->nickname,
                        'sex'               => $data->sex,
                        'bdate'             => date("Y-m-d H:i:s", strtotime($data->bdate))
                    ));

                    header("Location: http://".$_SERVER['HTTP_HOST'].'/control');
                };

            }; break;
        };
    };
}

function oauthFB(){
    $oauth_client_id      = '387660687942312';
    $oauth_secure_key     = '61d98f655bb2e79a4da0e8ff76af1306';
    $oauth_scope          = '';
    $redirect_uri         = urlencode('http://'.$_SERVER['HTTP_HOST'].'/control/auth/login?oauth&provider=fb');

    if(!isset($_GET["code"])){
        $this->doGetRequest('http://www.facebook.com/dialog/oauth', array(
            'client_id'     => $oauth_client_id,
            'scope'         => $oauth_scope,
            'redirect_uri'  => $redirect_uri
        ));
    }else{
        $access_token = $this->readGetRequest('https://graph.facebook.com/oauth/access_token', array(
            'client_id'     => $oauth_client_id,
            'redirect_uri'  => $redirect_uri,
            'client_secret' => $oauth_secure_key,
            'code'          => $_GET["code"]
        ), false);

        $graph_url = "https://graph.facebook.com/me?" . $access_token;
        $data = json_decode(file_get_contents($graph_url));

        if($data->gender == 'male'){
            $sex = '2';
        }elseif($data->gender == 'female'){
            $sex = '1';
        };

        $this->registerFromOAuth('fb', array(
            'uid'               => $data->id,
            'first_name'        => $data->first_name,
            'last_name'         => $data->last_name,
            'name'              => $data->name,
            'login'             => $data->username,
            'sex'               => $sex,
            'bdate'             => date("Y-m-d H:i:s", strtotime($data->birthday))
        ));

        header("Location: http://".$_SERVER['HTTP_HOST'].'/control');
    };
}
