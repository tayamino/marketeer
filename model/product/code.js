var Catalog = {
    product: function (item) {
        var block  = '<div class="col"><div class="card shadow-sm">';

        block += '<img class="bd-placeholder-img card-img-top" src="'+item.logo+'" width="100%" height="160" role="img" />';
        block += '<div class="card-body">';
        block += '<p class="card-text">'+item.name+'</p>';
        block += '<div class="d-flex justify-content-between align-items-center"><div class="btn-group">';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-primary"><i class="fa fa-code-fork"></i></button>';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></button>';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></button>';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';
        //block += '</div><small class="text-muted">'+item.name+'</small></div></div></div></div>';
        block += '</div><small class="text-muted"><i class="fa fa-'+item.icon+'"></i></small></div></div></div></div>';

        $('.preview').html(block);

        return block;
    },
};

/*********************************************************************************************/

var Editors = {
    product: function (item) {
        return Manager.views[Manager.daten.name].form;

        $.ajax({
            success: function (src) {
                var res;

                res = src;

                $('#modalEditor div.modal-body').html(res);
            },
            dataType: 'html',
            url: "/model/"+Manager.daten.name+"/form.html",
        });
        $.ajax({
            success: function (src) {
                var res;

                res = src;

                $('#modalEditor div.modal-foot').html(res);
            },
            dataType: 'html',
            url: "/model/"+Manager.daten.name+"/tool.html",
        });
    },
};

/*********************************************************************************************/

var Viewing = {
    product: function (item) {
        return Manager.views[Manager.daten.name].view;

        $('.details h3.product-title').html(item.name);
        $('.details p.product-description').html(item.description);

        //item.aggregateRating = item.aggregateRating || {};
        //item.aggregateRating.reviewCount = item.aggregateRating.reviewCount || 0;
        item.aggregateRating = { reviewCount: 0 };
        
        $('.details span.review-no').html(item.aggregateRating.reviewCount+" reviews");
        
        item.offers = item.offers || { price: 0, priceCurrency: "USD" };
        
        $('.details h4.price span').html(item.offers.price+' '+item.offers.priceCurrency);
        
        head='<li class="active"><a data-target="#cover" data-toggle="tab"><img src="'+item.logo+'" /></a></li>';
        body='<div class="tab-pane active" id="cover"><img src="'+item.logo+'" /></div>';

        head+='<li><a data-target="#image" data-toggle="tab"><img src="'+Endpoint.conn.link+item.image+'" /></a></li>';
        body+='<div class="tab-pane" id="image"><img src="'+Endpoint.conn.link+item.image+'" /></div>';
 
        $('.preview .preview-thumbnail').html(head);
        $('.preview .preview-pic').html(body);
    },
};

/*********************************************************************************************/

Manager.daten.demo['product'] = [
    "raspberrypi-4-1gb",
    "raspberrypi-4-2gb",
    "raspberrypi-4-4gb",
    "raspberrypi-4-compute-module",
    "raspberrypi-400",
];

Manager.boots(Endpoint.args.name || "product");
