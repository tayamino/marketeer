function render_product (alias) {
    for (var i=0 ; i<Endpoint.vars.shop.length ; i++) {
        if (Endpoint.vars.shop[i].text==alias) {
            var item = Endpoint.vars.shop[i].data;
            
            $('.details h3.product-title').html(item.name);
            $('.details p.product-description').html(item.description);
            
            $('.details span.review-no').html(item.aggregateRating.reviewCount+" reviews");
            
            $('.details h4.price span').html(item.offers.price+' '+item.offers.priceCurrency);
            
            head='<li class="active"><a data-target="#cover" data-toggle="tab"><img src="'+Endpoint.vars.shop[i].logo+'" /></a></li>';
            body='<div class="tab-pane active" id="cover"><img src="'+Endpoint.vars.shop[i].logo+'" /></div>';

            head+='<li><a data-target="#image" data-toggle="tab"><img src="'+Endpoint.conn.link+item.image+'" /></a></li>';
            body+='<div class="tab-pane" id="image"><img src="'+Endpoint.conn.link+item.image+'" /></div>';

            $('.preview .preview-thumbnail').html(head);
            $('.preview .preview-pic').html(body);
        }
    }
}
function formal_product (alias) {
}


$('#product_lister .btn-warning').click(function (ev) {
    var uid = $(ev.currentTarget).attr('data-uid');
    
    formal_product(uid);
    
    $('#modalEditor').modal('show');
});
$('#product_lister .btn-success').click(function (ev) {
    var uid = $(ev.currentTarget).attr('data-uid');
    
    render_product(uid);
    
    $('#modalViewer').modal('show');
});
$('#product_lister .btn-warning').click(function (ev) {
    var uid = $(ev.currentTarget).attr('data-uid');
    
    formal_product(uid);
    
    $('#modalEditor').modal('show');
});
$('#product_lister .btn-danger').click(function (ev) {
    if (confirm('Do you really want to delete ?')) {
        console.log("Delete folder : ",uid);
    }
});
$('#tools_create').click(function (ev) {
    $.ajax({
        success: function (src) {
            var res;

            res = src;

            $('div.modal-body').html(res);
        },
        dataType: 'html',
        url: "/model/product/form.html",
    });
    $('#modalEditor').modal('show');
});

var demo = [
    "raspberrypi-4-1gb",
    "raspberrypi-4-2gb",
    "raspberrypi-4-4gb",
    "raspberrypi-4-compute-module",
    "raspberrypi-400",
];
var curr = null;
var left = [];
var shop = {};
var keys = [];

$('#tools_loader').click(function (ev) {
    left = demo;

    curr = null;

    shop_loading(null);

    /*
    Endpoint.ajax('shop', "", function (data) {
        localStorage['shopping'] = JSON.stringify(data);
    });
    //*/
});

function shop_ajouter (meta,data) {
    keys.push(meta);

    shop[meta] = data;
}

function shop_loading (data) {
    if (curr!=null) shop_ajouter(curr,data);

    if (left.length) {
        curr = left.pop();

        $.ajax({
            success: shop_loading,
            dataType: 'json',
            url: "/drive/list/product/"+curr+".json",
            method: "GET",
        });
    } else {
        curr = null;

        shop_refresh(curr);
    }
}

function shop_refresh () {
    for (var i=0 ; i<keys.length ; i++) {
        var item = shop[keys[i]];

        //item.logo = Endpoint.conn.link+'/shop/'+item.name+'.png';

        item.logo = '/drive/shop/'+keys[i]+'.png';
    
        block  = '<div class="col"><div class="card shadow-sm">';
        block += '<img class="bd-placeholder-img card-img-top" src="'+item.logo+'" width="100%" height="160" role="img" />';
        block += '<div class="card-body">';
        block += '<p class="card-text">'+item.name+'</p>';
        block += '<div class="d-flex justify-content-between align-items-center"><div class="btn-group">';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-primary"><i class="fa fa-code-fork"></i></button>';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></button>';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></button>';
        block += '<button data-uid="'+item.name+'" type="button" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';
        //block += '</div><small class="text-muted">'+item.name+'</small></div></div></div></div>';
        block += '</div><small class="text-muted"><i class="fa fa-'+item.icon+'"></i></small></div></div></div></div>';
    
        $('#product_lister').append(block);
    }
}

function shop_backup () {
    localStorage['productName'] = JSON.stringify(keys);
    localStorage['productItem'] = JSON.stringify(shop);
}

function shop_initial () {
    keys = JSON.parse(localStorage['productName'] || '[]');
    shop = JSON.parse(localStorage['productItem'] || '{}');

    shop_refresh();
}

$('#tools_backup').click(function (ev) {
    shop_backup();

    alert("Data stored to localStorage !");

    console.log("localStorage = ", localStorage);
});

shop_initial();
