var Manager = {
    field: function (alias) {
        var resp = null, item;

        for (var i=0 ; resp==null && i<Manager.daten.keys.length ; i++) {
            item = Manager.daten.item[Manager.daten.keys[i]];
    
            if (item.name==alias) {
                resp = item;
            }
        }

        return resp;
    },
    pager: function () {
        if (Manager.daten.keys.length==0) {
            $('#listing').html("Nothing to show, Yet !");
        } else {
            $('#listing').html("");

            for (var i=0 ; i<Manager.daten.keys.length ; i++) {
                var item = Manager.daten.item[Manager.daten.keys[i]];
    
                //item.logo = Endpoint.conn.link+'/shop/'+item.name+'.png';
    
                item.logo = '/drive/shop/'+Manager.daten.keys[i]+'.png';

                $('#listing').append(Catalog.product(item));
            }
        }
        Manager.event($('#listing'));
    },
    event: function (root) {
        $('#listing .btn-primary').click(function (ev) {
            var uid = $(ev.currentTarget).attr('data-uid');
            
            var obj = Manager.field(uid);

            var web = Catalog.product(obj);
            
            $('#modalViewer').modal('show').find(".modal-body").html(web);
        });
        $('#listing .btn-success').click(function (ev) {
            var uid = $(ev.currentTarget).attr('data-uid');
            
            var obj = Manager.field(uid);

            var web = Viewing.product(obj);
            
            $('#modalViewer').modal('show').find(".modal-body").html(web);
        });
        $('#listing .btn-warning').click(function (ev) {
            var uid = $(ev.currentTarget).attr('data-uid');
            
            var obj = Manager.field(uid);

            var web = Editors.product(obj);
            
            $('#modalEditor').modal('show').find(".modal-body").html(web);
        });
        $('#listing .btn-danger').click(function (ev) {
            if (confirm('Do you really want to delete ?')) {
                console.log("Delete entry : ",uid);
            }
        });
    },
    model: {
        
    },
    daten: {
        exec: function (data) {
            if (Manager.daten.curr!=null) {
                Manager.daten.keys.push(Manager.daten.curr);

                Manager.daten.item[Manager.daten.curr] = data;
            };
        
            if (0 < Manager.daten.left.length) {
                Manager.daten.curr = Manager.daten.left.pop();
        	
                console.log("XHR : ", Manager.daten.curr, data);

                $.ajax({
                    success: Manager.daten.exec,
                    dataType: 'json',
                    url: "/drive/list/"+Manager.daten.name+"/"+Manager.daten.curr+".json",
                    method: "GET",
                });
            } else {
                Manager.daten.curr = null;
        
                Manager.pager(Manager.daten.curr);
            }
        },
        name: 'product',
        demo: {},
        keys: [],
        item: {},
        dest: [],
        left: [],
        curr: null,
    },
    boots: function (alias) {
        Manager.daten.name = alias || "product";

        Manager.daten.keys = JSON.parse(localStorage['schemaName'] || '[]');
        Manager.daten.item = JSON.parse(localStorage['schemaItem'] || '{}');
    	
        Manager.pager();
    },
    weave: function () {
        localStorage['schemaName'] = JSON.stringify(Manager.daten.keys);
        localStorage['schemaItem'] = JSON.stringify(Manager.daten.item);
    },
    level: null,
    graph: null,
    store: null,
    think: null,
    views: {},
};

/*********************************************************************************************/

$('#tools_static').click(function (ev) {
    Manager.daten.left = Manager.daten.demo[Manager.daten.name];

    Manager.daten.curr = null;

    Manager.daten.exec(null);

    console.log("Static Data as JSON-LD from : ",Manager.daten.left);
});

$('#tools_browse').click(function (ev) {
    console.log("Not implemented !");

    alert("Not implemented !");
});

$('#tools_fetchs').click(function (ev) {
    var link = prompt("Enter the link to fetch from :");

    console.log("Fetching JSON-LD from : ",link);
});

$('#tools_upload').click(function (ev) {
    console.log("Not implemented !");

    alert("Not implemented !");
});

/*********************************************************************************************/

$('#tools_delete').click(function (ev) {
    if (confirm("Are you sure ?")==true) {
        console.log("Resetting JSON-LD to ashes ...");

        Manager.daten.curr = null;
        Manager.daten.left = [];

        Manager.daten.item = {};
        Manager.daten.keys = [];

        localStorage['schemaName'] = JSON.stringify(Manager.daten.keys);
        localStorage['schemaItem'] = JSON.stringify(Manager.daten.item);
    }

    Manager.pager();
});

$('#tools_backup').click(function (ev) {
    Manager.weave();

    alert("Data stored to localStorage !");

    console.log("localStorage = ", localStorage);
});

$('#tools_loader').click(function (ev) {
    Manager.pager();
});

$('#tools_create').click(function (ev) {
    $.ajax({
        success: function (src) {
            var res;

            res = src;

            $('#modalEditor div.modal-body').html(res);
        },
        dataType: 'html',
        url: "/model/"+Manager.daten.name+"/form.html",
    });
    $.ajax({
        success: function (src) {
            var res;

            res = src;

            $('#modalEditor div.modal-foot').html(res);
        },
        dataType: 'html',
        url: "/model/"+Manager.daten.name+"/tool.html",
    });

    $('#modalEditor').modal('show');
});

/*********************************************************************************************/

var tplNames = ['form','tool','view'];

function load_template (alias) {
    $.ajax({
        success: function (value) {
            if (!(Manager.views[Manager.daten.name])) {
                Manager.views[Manager.daten.name] = {};
            }

            Manager.views[Manager.daten.name][alias] = value;
        },
        dataType: 'html',
        url: "/model/"+Manager.daten.name+"/"+alias+".html",
    });
}
for (var i=0 ; i<tplNames.length ; i++) { load_template(tplNames[i]); }

setTimeout(function () {
    var head = document.getElementsByTagName('head')[0];

    // Creating link element
    var style = document.createElement('link');
    style.href = "/model/"+Endpoint.args.name+"/code.css";
    style.type = 'text/css';
    style.rel = 'stylesheet';
    head.append(style);

    var src = document.createElement('script'); src.setAttribute('src', "/model/"+Endpoint.args.name+"/code.js"); document.body.appendChild(src);

    document.getElementById("page-title").innerHTML = "Schema.org :: " + Endpoint.args.name;

    document.title = document.getElementById("page-title").innerHTML + " [Antikythera]";
}, 1000);
