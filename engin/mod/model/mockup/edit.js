$(function () {
    /*
    var args = window.location.search.substring(1).split('&');

    var arguments = {};

    for (i=0 ; i<args.length ; i++) {
        pair = args[i].split('=');
        
        arguments[pair[0]] = pair[1];
    }

    if (arguments.page.length) $("#pagename").val(arguments.page);
    if (arguments.link.length) $("#web_link").val(arguments.link);
    //*/

    $('#extracts').click(function (ev) {
        endpoint_ajax('mock', "link="+$("#web_link").val(), function (data) {
            $("#sourcing").val(data);
            //$("#preview").html(data);
        });
    });

    if (localStorage['tempMockup']) {
        var data = JSON.parse(localStorage['tempMockup']);
        
        $("#pagename").val(data.name);
        $("#web_link").val(data.link);
        $("#sourcing").val(data.html);
        
        localStorage['tempMockup'] = null;
    }
});

