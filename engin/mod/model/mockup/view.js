Endpoint.ajax('mock', "", function (data) {
    if (Endpoint.args.page) {
        jQuery.ajax({
            success: function (source) {
                $('#mockup_lister').html('<div class="col-md-12"><textarea id="source" rows="17" class="form-control">'+source+'</textarea></div>');
            },
            url: Endpoint.conn.link+"/mock/"+Endpoint.args.name+"/"+Endpoint.args.page+".html"
        });
        console.log(Endpoint.conn.link+"/mock/"+Endpoint.args.name+"/"+Endpoint.args.page+".html");
    } else {
        for (var i=0 ; i<data.length ; i++) {
            if (data[i].name==Endpoint.args.name) {
                for (var j=0 ; j<data[i].page.length ; j++) {
block  = '<div class="col"><div class="card shadow-sm">';
block += '<img class="bd-placeholder-img card-img-top" src="'+data[i].logo+'" width="100%" height="225" role="img" />';
block += '<div class="card-body">';
block += '<p class="card-text">'+data[i].page[j]+'</p>';
block += '<div class="d-flex justify-content-between align-items-center"><div class="btn-group">';
block += '<a href="'+Endpoint.conn.link+'/mock/'+data[i].name+'/'+data[i].page[j]+'.html" class="btn btn-sm btn-success" target="_new"><i class="fa fa-eye"></i></a>';
block += '<a href="/model/mockup/view.html?name='+data[i].name+'&page='+data[i].page[j]+'" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>';
//block += '</div><small class="text-muted">'+data[i].name+'</small></div></div></div></div>';
block += '</div><small class="text-muted">'+data[i].page[j]+'</small></div></div></div></div>';

$('#mockup_lister').append(block);
                }
            }
        }
    }
});
