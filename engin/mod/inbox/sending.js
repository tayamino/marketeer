function get_server (index) {
    var key = ['type','host','port','username','password','limit','from_email'];

    var obj = {
        //index: index,
    };
    for (j=0 ; j<key.length ; j++) {
        obj[key[j]] = $('#serversContent *[name="'+key[j]+'[]"]')[index].value;
    }
    obj.port = parseInt(obj.port) || 0;
    obj.limit = parseInt(obj.limit) || 0;

    return obj;
}
function all_server () {
    var nbr = $("ul#servers.nav-tabs li.nav-item").length-1; //think about it ;)
    var lst = [];

    for (i=0 ; i<nbr ; i++) {
        lst.push(get_server(i));
    }

    return lst;
}
function demo_server () {
    var lst = [];

    lst.push({ limit: 500, host:"mail.ovh.net", port:587, type:'tls', username:"anonymous", password:"thesecret" });
    lst.push({ limit: 200, host:"smtp.gmail.com", port:465, type:'ssl', username:"anonymous", password:"thesecret" });
    lst.push({ limit: 200, host:"smtp.hotmail.com", port:465, type:'ssl', username:"anonymous", password:"thesecret" });

    localStorage['smtpServers'] = JSON.stringify(lst);
}
function load_server () {
    Endpoint.name('serv',null);

    try {
        Endpoint.vars['serv'] = JSON.parse(localStorage['smtpServers']);
    } catch (ex) {
        Endpoint.vars['serv'] = [];
    }

    if (Endpoint.vars['serv']) {
        var resp,item;

        for (i=0 ; i<Endpoint.vars['serv'].length ; i++) {
            item = Endpoint.vars['serv'][i];

            item.index = i;
            item.order = i+1;

            $('a.add-server').closest('li').before('<li class="nav-item" role="presentation"><span class="nav-link" data-toggle="tab" data-bs-toggle="tab" data-bs-target="#server_'+item.order+'" role="tab" aria-controls="server_'+item.order+'" aria-selected="false"><span>Server #'+item.order+'</span><i class="fa fa-close"></i></span></li>');

            resp = Endpoint.view('#server_form', item);

            resp = '<div id="server_'+item.order+'" class="tab-pane fade" role="tabpanel">'+resp+'</div>';

            $('#serversContent').append(resp);
        }
    }
}
function save_server () {
    Endpoint.vars['serv'] = all_server();

    localStorage['smtpServers'] = JSON.stringify(Endpoint.vars['serv']);
}
function mapping_start () {
    var target = $('#contact').val().split('\n');
    var emails = [];
    var server = all_server();
    var vector = [];
    var number = 0;
    var answer = [];

    for (i=0 ; i<target.length ; i++) {
        if (target[i].length) {
            emails.push(target[i]);
        }
    }

    for (i=0 ; i<server.length ; i++) {
        vector.push(server[i].limit);
        number += server[i].limit;
    }

    cycle = parseInt(emails.length / number);

    for (i=0 ; i<cycle ; i++) {
        for (j=0 ; j<vector.length ; j++) {
            answer.push({ index: j, limit: vector[j], usage: vector[j] });
        }
    }

    cycle = emails.length % number;

    while (0<cycle) {
        for (j=0 ; j<vector.length && 0<cycle ; j++) {
            if (cycle<vector[j]) {
                answer.push({ index: j, limit: vector[j], usage: cycle });
            } else {
                answer.push({ index: j, limit: vector[j], usage: vector[j] });
            }

            cycle -= vector[j];
        }
    }

    $('#limit').val(number);
    $('#sleep').val(emails.length);

    multi = parseInt($('#multi').val());
    //multi = 4096;
    queue = d3.queue(multi);

    /*
    var triggerEl = document.querySelector('#targets button[data-bs-target="#routing"]');
    bootstrap.Tab.getInstance(triggerEl).show(); // Select tab by name
    */
    //*
    smtp_counter = 0;

    for (i=0 ; i<answer.length ; i++) {
        for (j=0 ; j<answer[i].usage ; j++) {
            /*
            args = [
                'type='server[answer[i].index].type,
                'host='server[answer[i].index].host,
                'port='server[answer[i].index].host,
                'username='server[answer[i].index].username,
                'password='server[answer[i].index].password,
            ].join('&');
            //*/

            queue.defer(function (data, callback) {
    d3.request(Endpoint.conn.link+'/?acte=smtp').post(JSON.stringify(data), function (error, result) {
        if (!error && result) {
            var entry = JSON.parse(result.response);
            if (entry!=null) {
                $('#evolution').append(entry.number+' : '+entry.index+' : '+emails[entry.index]+'<br>');
                percent = (entry.index / emails.length) * 100;
                $('#progress').attr('aria-valuenow',percent).css('width',percent+'%');
                $('#evolution')[0].scroll(0,1000000000000);
            }
        }
        return callback(error, result);
    });
            },{
                address: emails[smtp_counter],
                message: $('#message').val(),
                content: $('#content').val(),

                number: answer[i].index,
                server: server[answer[i].index],

                index:  smtp_counter,
                total:  emails.length,
                /*
                host: document.getElementById("host").value,
                port: document.getElementById("port").value,
                type: document.getElementById("type").value,

                username: document.getElementById("username").value,
                password: document.getElementById("password").value,

                index:index,
                total:procs.length,
                from_mail:document.getElementById("from_mail").value,
                from_name:document.getElementById("from_name").value,
                subject:document.getElementById("subject").value,
                email:value,
                message:document.getElementById('content_text').value,
                content:document.getElementById('content_html').value,
                router:router,
                //*/
            });
            smtp_counter += 1;
        }
    }
    //*/
    queue.awaitAll(function (error, results) {
        alert("Sending done !");
    });

    console.log("Mapping : ",emails.length,number,answer);
}
var smtp_counter = 0;
jQuery(function ($) {
    email=""; limit=2050;
    for (i=0 ; i<limit ; i++) {
        email += "tayamino@gmail.com\n";
    }
    $('#contact').val(email);

    load_server();

    $(".nav-tabs").on("click", "i", function (e) {
        e.preventDefault();
        var anchor = $(this).parent('span.nav-link');
        $(anchor.attr('data-bs-target')).remove();
        anchor.parent('.nav-item').remove();
        $(".nav-tabs li").children('span').first().click();
    }).on("click", "span", function(e){
          try {
              $(this).tab('show');
          } catch (ex) {
              console.log(ex);
          }
          e.preventDefault();
    });

    $('.add-server').click(function(e) {
        e.preventDefault();
        var id = $("ul#servers.nav-tabs li.nav-item").length; //think about it ;)
        $(this).closest('li').before('<li class="nav-item" role="presentation"><span class="nav-link" data-toggle="tab" data-bs-toggle="tab" data-bs-target="#server_'+id+'" role="tab" aria-controls="server_'+id+'" aria-selected="false"><span>Server #'+id+'</span><i class="fa fa-close"></i></span></li>');

        resp = Endpoint.view('#server_form', {
            index: id,
        });

        resp = '<div id="server_'+id+'" class="tab-pane fade" role="tabpanel">'+resp+'</div>';

        $('.tab-content').append(resp);
    });
    /*
    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        var target = $(e.target).attr("href");
        alert('clicked on tab' + target);
    });
    //*/
    $('#tool_start').click(function(e) {
        mapping_start(e);
    });
    $('#tool_backup').click(save_server);
    $('#tool_mockup').click(demo_server);
});

var queue = null;
var waits = null;
var procs = null;
var clock = null;
var index = 0;
var limit = 1;
var total = 0;
var sleep = 1000;

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
var phoneReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

var Manager = {
    begins: function () {
        if (!Manager.fields()) {
            return;
        }

        waits = [];

        var source = document.getElementById("contact").value.split("\n");

        for (i=0 ; i<source.length ; i++) {
            value = source[i].trim();

            if (value!="" && Manager.checks(value)) {
                waits.push(value);
            }
        }

        jQuery("#envoyerbtn").attr("disabled", "disabled");

        total = waits.length;

        jQuery("#progress").empty();

        limit = parseInt(document.getElementById("limit").value);

        sleep = parseFloat(document.getElementById("sleep").value) * 1000;

        Manager.looper();
    },
    looper: function () {
        queue = d3.queue(multi);

        var offset = index * limit;

        jQuery("#envoyerbtn").attr("class","btn btn-success btn-sm");

        procs = waits.slice(0,limit);

        waits = waits.slice(limit);

        console.log("loop : ",offset,index,limit,total);

        jQuery("#compteur").text(offset+" / "+total);

        index += 1;

        for (i=0 ; i<procs.length ; i++) {
            Manager.listed(i,procs[i]);
        }

        queue.awaitAll(Manager.handle);
    },
    finish: function () {
        jQuery("#envoyerbtn").attr("disabled", false);
        jQuery("#compteur").text("DONE");
        jQuery("#envoyerbtn").attr("class","btn btn-default btn-sm");
    },
    handle: function (error, results) {
        //if (error) throw error;

        if (waits.length) {
            //*
            jQuery("#compteur").text("Sleeping for "+sleep+" ms");

            clock = setTimeout(function () { clearTimeout(clock); Manager.looper(); },sleep);

            jQuery("#envoyerbtn").attr("class","btn btn-default btn-sm");
            //*/
        } else {
            Manager.finish();
        };
    },
    result: function (error, result, callback) {
        var response = null;

        try {
            response = JSON.parse(result.response);
        } catch (err) {
            response = {
                error : err,
                echo  : result.response,
            };
        }

        jQuery("#logging").val(response.echo);

        error = error || response.error;

        jQuery("#progress").append('<div class="col-lg-3">' + (response.index+1).toString() + '/' + response.total + '</div><div class="col-lg-6">' + response.email + '</div>');

        if (error){
            jQuery("#errorlog").append('<p>'+JSON.stringify(error)+'</p>');

            jQuery("#progress").append('<div class="col-lg-1"><span class="label label-danger">failed</span></div>');
        } else {
            jQuery("#progress").append('<div class="col-lg-1"><span class="label label-success">success</span></div>');
        }

        jQuery("#progress").append('<br>');

        window.scrollTo(0,document.body.scrollHeight);

        callback(error, response);
    },
};
