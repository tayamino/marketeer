$(function () {
    $('#configure').click(function (ev) {
        $('#modalConfigure').modal('show');
    });
});


var CONCURRENT = 4;

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function checkInbox() {
    if (!highlight('host') || !highlight('port') || !highlight('type')) {
        return;
    }

    var q = d3.queue(CONCURRENT);

    jQuery("button").attr("disabled", true);

    q.defer(function (data, callback) {
        d3.request("http://localhost:3000/?acte=imap")
          .header("Content-Type", "application/json")
          .post(JSON.stringify(data), function(error, result) {
            if (result) {
                var response = JSON.parse(result.response);

                error = error || response.error;
                
                var entry = '<th scope="row">1</th>';

                if (error){
                    entry += '<td colspan="3">'+error.toString()+'</td>';
                } else {
                    entry += '<td>'+response.addr+'</td>';
                    entry += '<td>'+response.time+'</td>';
                    entry += '<td>'+response.user+'</td>';
                    entry += '<td>'+response.data+'</td>';
                }

                jQuery("#results tbody").prepend("<tr>"+entry+"</tr>");

                callback(error, response);
            } else {
                callback(error, result);
            }
        });
    },{
        host: document.getElementById("host").value,
        port: document.getElementById("port").value,
        type: document.getElementById("type").value,

        username: document.getElementById("username").value,
        password: document.getElementById("password").value,
    });

    q.awaitAll(finishInbox);
}
function finishInbox(error, results) {
    //if (error) throw error;

    jQuery("button").attr("disabled", false);

    //document.querySelector("#result").textContent = results.toString();
}

