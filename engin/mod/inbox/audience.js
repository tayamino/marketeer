function refresh_audience () {
    var ctrl = $('#contact')[0];
    var args = [];
    
    for (i=0 ; i<ctrl.options.length ; i++) {
        if (ctrl.options[i].selected) {
            args.push('list[]='+ctrl.options[i].value);
        }
    }

    Endpoint.ajax('info', args.join('&'), function (data) {
        for (var i=0 ; i<data.length ; i++) {
            for (var j=0 ; i<data[i].list.length ; i++) {
                block  = '<tr>';
                block += '<td>'+data[i].list[j].alias+'</td>';
                block += '<td>'+data[i].list[j].title+'</td>';
                block += '<td>'+data[i].list[j].email+'</td>';
                block += '<td>'+data[i].list[j].phone+'</td>';
                block += '<td>'+data[i].list[j].birth+'</td>';
                block += '<td>'+data[i].list[j].where+'</td>';
                block += '<td>'+data[i].list[j].links+'</td>';
                block += '<td>'+data[i].list[j].doing+'</td>';
                block += '<td>'+data[i].list[j].oauth.facebook+'</td>';
                block += '<td>'+data[i].list[j].oauth.twitter+'</td>';
                block += '<td>'+data[i].list[j].oauth.linkedin+'</td>';
                block += '</tr>';
                $('#lister').append(block);
            }
        }
    });
}
jQuery(function ($) {
    Endpoint.ajax('info', "", function (data) {
        for (var i=0 ; i<data.length ; i++) {
            item  = '<option class="'+data[i].type+'" value="'+data[i].name+'">';
            item += '<i class="fa fa-'+data[i].icon+'"></i>&nbsp;'+data[i].uuid;
            item += '</option>';
            $('#contact').append(item);
        }
    });
    $('#contact').change(function (ev) {
        refresh_audience();
    });
    /**************************************************************************/
    $('div.btn-toolbar .btn-danger').click(function (ev) {
        if (confirm('Do you really want to delete ?')) {
            console.log("Delete backend : ",uid);
        }
    });
    $('div.btn-toolbar .btn-warning').click(function (ev) {
        $('#modalEditor').modal('show');
    });
    $('div.btn-toolbar .btn-success').click(function (ev) {
        $('#modalEditor').modal('show');
    });
});

