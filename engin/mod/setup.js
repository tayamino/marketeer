function read_endpoint() {
    cfg = JSON.parse(localStorage['endpoint'] || '{}');
    
    if (!(cfg.host)) cfg.host = 'localhost';
    if (!(cfg.port)) cfg.port = 3000;
    if (!(cfg.type)) cfg.type = 'http';

    $('#endpointHost').val(cfg.host);
    $('#endpointPort').val(cfg.port);
    $('#endpointType').val(cfg.type);
}
function save_endpoint() {
    cfg = {
        host: $('#endpointHost').val() || 'localhost',
        port: $('#endpointPort').val() || 3000,
        type: $('#endpointType').val() || 'http',
    };

    cfg.link = cfg.type+'://'+cfg.host+':'+cfg.port;

    localStorage['endpoint'] = JSON.stringify(cfg);
}

jQuery(function ($) {
    $('button#saving').click(save_endpoint);

    $('#endpointForm input,#endpointForm select').keypress(save_endpoint);
    $('#endpointForm input,#endpointForm select').change(save_endpoint);
    
    read_endpoint();
});

$(function () {
    var args = window.location.search.substring(1).split('&');

    var arguments = {};

    for (i=0 ; i<args.length ; i++) {
        pair = args[i].split('=');
        
        arguments[pair[0]] = pair[1];
    }

    //if (arguments.page.length) $("#pagename").val(arguments.page);
    //if (arguments.link.length) $("#web_link").val(arguments.link);
    
    ctx = {
        library: Endpoint.vars.library.list,
        modules: Endpoint.vars.modules.list,
        renders: Endpoint.vars.renders,
        customs: Endpoint.vars.customs,
    };
    
    src = Endpoint.view('#tpl_servers',ctx);
    
    $('#servers').html(src);

    $("#serving").click(function (ev) {
        ctx = {
            library: Endpoint.vars.library.list,
            modules: [],
            renders: Endpoint.vars.customs,
            customs: $('#custom_server').val(),
        };

        for (var i=0 ; i<Endpoint.vars.modules.list.length ; i++) {
            k = Endpoint.vars.modules.list[i];

            if ($("#has_"+k).val()) {
                ctx.modules.push(k);
            }
        }
        
        src = Endpoint.view('#tpl_daemons',ctx);

        $('#php_output').val(src);

        $('#modalSourceCode').modal('show');

        //$('#custom_server').val();
    });
});
Endpoint.name('library',{
    curr: [],
    text: {},
    list: ['formats','helpers','library'],
});
Endpoint.name('modules',{
    curr: [],
    text: {},
    list: ['backend','contact','filesystem','indexing','mockups','networks','protocol','shopping','transport'],
});

for (i=0 ; i<Endpoint.vars.library.list.length ; i++) {
    k = Endpoint.vars.library.list[i];

    $('#tpl_servers').after('<script type="text/html" id="lib_'+k+'"></script>');

    $('#lib_'+k).load('/engin/php/'+k+'.php');
}
for (i=0 ; i<Endpoint.vars.modules.list.length ; i++) {
    k = Endpoint.vars.modules.list[i];

    $('#tpl_servers').after('<script type="text/html" id="mod_'+k+'"></script>');

    $('#mod_'+k).load('/engin/uni/'+k+'.php');
}

if (false) {
    var link = 'server';
    for (i=0 ; i<Endpoint.vars.library.list.length ; i++) {
        link += '&core[]='+Endpoint.vars.library.list[i];
    }
    for (i=0 ; i<Endpoint.vars.modules.list.length ; i++) {
        link += '&plug[]='+Endpoint.vars.modules.list[i];
    }
    Endpoint.ajax('code',link,function (data) {
        Endpoint.name('customs',data);
    });
}

function collect_settings () {
    Endpoint.vars.modules.name = [];

    for (i=0 ; i<Endpoint.vars.modules.list.length ; i++) {
        k = Endpoint.vars.modules.list[i];

        if ($('#has_'+k).val()) {
            Endpoint.vars.modules.name.push(k);
        }
    }
}
function collect_template () {
    for (i=0 ; i<Endpoint.vars.library.list.length ; i++) {
        k = Endpoint.vars.library.list[i];

        v = $('#lib_'+k).html();

        Endpoint.vars.library.text[k] = v;
    }
    for (i=0 ; i<Endpoint.vars.modules.list.length ; i++) {
        k = Endpoint.vars.modules.list[i];

        v = $('#mod_'+k).html();

        Endpoint.vars.modules.text[k] = v;
    }
}
setTimeout(function () {
    collect_settings();
    collect_template();
},1500);

