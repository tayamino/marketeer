function navigate (path) {
    path = path.replace('./','');

    Endpoint.ajax('dirs', "path="+path, function (data) {
        $('#viewer').html("");

        for (var i=0 ; i<data.length ; i++) {
block  = '<div class="col-md-3"><div class="card shadow-sm">';
if (data[i].type=='folder') {
    block += '<span class="directory" data-path="'+data[i].link+'">';
}
block += '<i class="fa card-icon fa-'+data[i].icon+' fa-4x"></i>';
if (data[i].type=='folder') {
    block += '</span>';
}
block += '<div class="card-body">';
block += '<p class="card-text">'+data[i].name+'</p>';
if (false) {
    block += '<div class="d-flex justify-content-between align-items-center"><div class="btn-group">';
    block += '<button type="button" class="btn btn-sm btn-outline-secondary"><i class="fa fa-eye"></i></button>';
    block += '<button type="button" class="btn btn-sm btn-outline-secondary"><i class="fa fa-pencil"></i></button>';
    //block += '</div><small class="text-muted">'+data[i].name+'</small></div></div></div></div>';
    block += '</div><small class="text-muted">'+data[i].size+'</small></div>';
}
block += '</div></div></div>';

$('#viewer').append(block);
        }

        $('h1#current_path').html(path);

        $('#viewer span.directory').click(function (ev) {
            var path = $(ev.currentTarget).attr('data-path');
            
            console.log(path);
            
            navigate(path);
        });
    });
}

$(function () {
    Endpoint.ajax('tree', "", function (data) {
        $('#forest').html(data);
        $('#explor').html(data);

        $('#forest').treed({openedClass:'fa-minus', closedClass:'fa-plus'});
        $('#explor').treed({openedClass:'fa-minus', closedClass:'fa-plus'});

        $('#forest span.directory').click(function (ev) {
            var path = $(ev.currentTarget).attr('data-path');
            
            console.log(path);
            
            navigate(path);
        });

        navigate('.');
    },null,'html');
    $('#tools_fetch').click(function (ev) {
        var link = prompt('Enter the link of the file to fetch :');

        console.log("Fetching link : ",link);
    });
    $('#tools_mkdir').click(function (ev) {
        var name = prompt('Enter the name of the folder to create :');

        console.log("Making folder : ",name);
    });
    $('#tools_touch').click(function (ev) {
        var name = prompt('Enter the name of the file to create :');

        console.log("Making file : ",name);
    });
    $('#tools_unzip').click(function (ev) {
        var path = prompt('Enter the name of the archive to extract :');

        console.log("Extracting archive : ",path);
    });
    $('#tools_copy').click(function (ev) {
        $('#modalExplorer').modal('show');
    });
    $('#tools_move').click(function (ev) {
        $('#modalExplorer').modal('show');
    });
    $('#tools_delete').click(function (ev) {
        if (confirm('Do you really want to delete ?')) {
            console.log("Delete folder : ",uid);
        }
    });
    $('#tools_archive').click(function (ev) {
        if (confirm('Do you really want to archive ?')) {
            console.log("Archive folder : ",uid);
        }
    });
});

