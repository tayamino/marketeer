jQuery(function ($) {
    if (Endpoint.args.scanner) {
        $('#scan-tab').tab('show');
    } else if (Endpoint.args.start && Endpoint.args.finish) {
        $('#cidr-tab').tab('show');
    } else if (Endpoint.args.addr) {
        if (0<=Endpoint.args.addr.search(':')) {
            $('#ipv6-tab').tab('show');
        } else {
            $('#ipv4-tab').tab('show');
        }
    } else {
        $('#info-tab').tab('show');
    }
});

Endpoint.ajax('cidr', "", function (data) {
    var tool = [{
        name: 'danger', icon: 'trash', text: "Delete"
    },{
        name: 'warning', icon: 'pencil', text: "Edit"
    },{
        name: 'success', icon: 'code', text: "Execute"
    },{
        name: 'dark', icon: 'recycle', text: "Initialize"
    },{
        name: 'primary', icon: 'upload', text: "Uploader"
    },{
        name: 'secondary', icon: 'download', text: "Download"
    }];
    var name = ['alias','begin','finis'];

    for (var i=0 ; i<data.length ; i++) {
        block  = '<th id="'+data[i].alias+'" scope="row">'+(i+1)+'</th>';

        for (var j=0 ; j<name.length ; j++) {
            block += '<td>'+data[i][name[j]]+'</td>';
        }
        block += '<td>';
        for (var j=0 ; j<tool.length ; j++) {
            block += '<span class="tool btn btn-sm btn-'+tool[j].name+'"><i class="fa fa-'+tool[j].icon+'"></i></span>';
        }
        block += '</td>';

        $('#networks').append('<tr>'+block+'</tr>');
    }
    //$('#networks').datatable();
});

