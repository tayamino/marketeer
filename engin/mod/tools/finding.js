function searching (kind,word) {
    if (kind!=null) {
        Endpoint.name('k',kind);
    } else {
        kind = Endpoint.args['k'];
    }

    if (word==null) {
        word = $('input[name="search"]').val();
    }
    
    Endpoint.name('t',word);

    $('h1.h2').html('"'+word+'"');

    $('input[name="search"]').attr('placeholder',word);

    $('header input').val(word);
    
    Endpoint.ajax('find', "q="+word+"&t="+kind, function (data) {
        Endpoint.name('d',data);
        
        var block = "";
        
        for (var i=0 ; i<data.length ; i++) {
            block += '<div class="search-result">';
            block += '<h3><a href="'+data[i].link+'">'+data[i].name+'</a></h3>';
            block += '<a href="'+data[i].link+'" class="search-link">'+data[i].link+'</a>';
            block += '<p>'+data[i].text+'</p>';
            block += '</div><div class="hr-line-dashed"></div>';
        }

        $('span.text-navy').html(data.length+' results found for:');

        $('#results').html(block);
    });
}
jQuery(function ($) {
    $('input[name="search"]').keypress(function (ev) {
        if (ev.originalEvent.charCode==13) {
            searching(null,null);
        }
    });
    $('#performs').click(function (ev) {
        searching(null,null);
    });
    $('#facets button').click(function (ev) {
        var kind = $(ev.currentTarget).attr('id');
        
        searching(kind,null);
    });

    var word = Endpoint.args.q.split('+').join(' ');
    
    searching(null,word);
});

