$(function() {
  $('#current input').keypress(function (ev) {
    if (ev.originalEvent.charCode==13) {
        Endpoint.ajax('exec','cmd='+$('#current .cmdline').val(),function (data) {
            block  = '<div id="input-line" class="input-line">';
            block += '<div class="prompt">[user@HTML5] <span class="clignoter">#</span></div><div>';
            block += '<input class="cmdline" readonly value="'+$('#current .cmdline').val()+'" />';
            block += '</div></div><output>';
            block += '<p>'+data.html+'</p>';
            block += '</output>';
            $('#results').append(block);
            $('#current .cmdline').val("");
        });
    }
  });
  
  /*
  // Initialize a new terminal object
  var term = new Terminal('#input-line .cmdline', '#container output');
  term.init();
  
  // Update the clock every second
  setInterval(function() {
    function r(cls, deg) {
      $('.' + cls).attr('transform', 'rotate('+ deg +' 50 50)')
    }
    var d = new Date()
    r("sec", 6*d.getSeconds())  
    r("min", 6*d.getMinutes())
    r("hour", 30*(d.getHours()%12) + d.getMinutes()/2)
  }, 1000);
  //*/
});

