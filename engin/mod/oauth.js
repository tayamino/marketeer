$(function () {
    var args = window.location.search.substring(1).split('&');

    var arguments = {};

    for (i=0 ; i<args.length ; i++) {
        pair = args[i].split('=');
        
        arguments[pair[0]] = pair[1];
    }

    if (arguments.provider) {
        $('#title').load("/oauth/"+arguments.provider+"/info.txt");
        $('#forms').load("/oauth/"+arguments.provider+"/form.html");
        $('#howto').load("/oauth/"+arguments.provider+"/help.md");
    } else (
        $('#title').html("OAuth : "+arguments.provider.capitalize());
        $('#forms').load("/oauth/"+arguments.provider+"/form.html");
        $('#howto').load("/oauth/"+arguments.provider+"/help.md");
    )

    //if (arguments.page.length) $("#pagename").val(arguments.page);
    //if (arguments.link.length) $("#web_link").val(arguments.link);
    
    ctx = {
        library: Endpoint.vars.library,
        modules: Endpoint.vars.modules,
        renders: Endpoint.vars.renders,
        customs: Endpoint.vars.customs,
    };
    
    src = Endpoint.view('#tpl_servers',ctx);
    
    $('#servers').html(src);
});

