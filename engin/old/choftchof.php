<?php

function read_inbox ($host, $port, $type, $username, $password, $search=null) {
    if ($search==null) {
        $search = "";
    }

    //Attempt to connect using the imap_open function.
    $imapResource = imap_open('{'.$host.':'.$port.'/'.$type.'}', $username, $password);

    //If the imap_open function returns a boolean FALSE value,
    //then we failed to connect.
    if($imapResource === false){
        //If it failed, throw an exception that contains
        //the last imap error.
        throw new Exception(imap_last_error());
    }

    //If we get to this point, it means that we have successfully
    //connected to our mailbox via IMAP.

    //Lets get all emails that were received since a given date.
    $search = 'UNSEEN SINCE "' . date("j F Y", strtotime("-7 days")) . '"';
    $emails = imap_search($imapResource, $search);
    
    //If the $emails variable is not a boolean FALSE value or
    //an empty array.
    
    $resp = [];
    
    if(!empty($emails)){
        //Loop through the emails.
        foreach($emails as $email){
            $header = imap_headerinfo($imapResource, $email);
            echo 'From: ' . $overview->from . '<br><br>';
            //Get the body of the email.
            $message = imap_fetchbody($imapResource, $email, 1, FT_PEEK);
                $item = [
                    "numb" => $header-uid,
                    "time" => $header->udate, //date("F j, Y, g:i a", $header->udate),
                ];
            $line = implode("IP: ",$header->Subject,1);
                $item["addr"] = $line[1];
                    //Fetch an overview of the email.
                    $overview = imap_fetch_overview($imapResource, $email);
                    $overview = $overview[0];
                    //Print out the subject of the email.
                    echo '<b>' . htmlentities($overview->subject) . '</b><br>';
                    //Print out the sender's email address / from email address.
                $item["data"] = [];
                    foreach (implode("\n",$message) as $line) {
                        $line = implode(":",$line,1);
    
                        $item["data"][$line[0]] = $line[1];
                    }
                $item["user"] = null;
            $resp[] = $item;
        }
    }

    return $resp;
}

//##########################################################################################

$config = [
    "localhost" => [
        "host" => "localhost",
        "port" => 143,
        "type" => "notls",
    ],
    "gmail.com" => [
        "host" => "imap.gmail.com",
        "port" => 993,
        "type" => "ssl",
    ],
    "hotmail.com" => [
        "host" => "imap-mail.outlook.com",
        "port" => 993,
        "type" => "ssl",
    ],
];

if ($_SERVER['REQUEST_METHOD']=='POST') {
    $data = json_decode(file_get_contents("php://input"),true);

    $resp = [];

    if (!strlen($data["host"]) && !strlen($data["port"]) && !strlen($data["type"])) {
        $domain = preg_replace("/([^@]*).*/", "$2", $data['email']);
        
        if (array_key_exists($domain, $config)) {
            $data = array_merge($data,$config[$domain]);
        }
    }

    if (!strlen($data["host"]) && !strlen($data["port"]) && !strlen($data["type"])) {
        $resp = [
            "error" => "Missing configuration",
        ];
    } else {
        $resp = read_inbox($data['host'],$data['port'],$data['type'],$data['username'],$data['password']);
    }

    echo json_encode(array_merge($data,$resp));
} else {
?>
<!doctype html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta charset="utf-8">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/cosmo/bootstrap.min.css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <title>GRINI</title>
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Hostname" id="host" value="localhost">
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" placeholder="Port" id="port" value="143">
                </div>
                <div class="col-md-3">
                    <select id="router" name="type" class="form-control" placeholder="IMAP type">
                        <option value="notls">no-TLS</option>
                        <option value="tls">TLS</option>
                        <option value="ssl">SSL</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Username" id="username" value="">
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Password" id="password" value="">
                </div>
                <div class="col-md-4">
                    <button type="button" class="btn btn-default btn-sm" onclick="checkInbox()">CHECK</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="results" class="table">
                        <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Address</th>
                              <th scope="col">Submit On</th>
                              <th scope="col">User-Agent</th>
                              <th scope="col">Payload</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>
                              <td>xxxxx</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
    <style>
.clignote
{
  animation: Test 1s infinite;
  color:blue;
}
.success
{
  color:green;
  font-weight: bold
}
.error
{
  color:red;
  font-weight: bold
}
.devider
{
 height: 20px;
 width: 100%;
}
.normal
{
  color:black;
  
}
@keyframes Test{
    0%{opacity: 1;}
    50%{opacity: 0;}
    100%{opacity: 1;}
}
    </style>
    <script src="https://d3js.org/d3-collection.v1.min.js"></script>
    <script src="https://d3js.org/d3-dispatch.v1.min.js"></script>
    <script src="https://d3js.org/d3-dsv.v1.min.js"></script>
    <script src="https://d3js.org/d3-request.v1.min.js"></script>
    <script src="https://d3js.org/d3-queue.v3.min.js"></script>
    <script>
var CONCURRENT = 4;

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function highlight(name) {
    var obj = document.getElementById(name);

    if (obj.value.trim().length==0) {
        alert("Empty field : "+name);

        obj.focus();

        return false;
    }

    return true;
}
function checkInbox() {
    if (!highlight('host') || !highlight('port') || !highlight('type')) {
        return;
    }

    var q = d3.queue(CONCURRENT);

    jQuery("button").attr("disabled", true);

    q.defer(function (data, callback) {
        d3.request("?")
          .header("Content-Type", "application/json")
          .post(JSON.stringify(data), function(error, result) {
            if (result) {
                var response = JSON.parse(result.response);

                error = error || response.error;
                
                var entry = '<th scope="row">1</th>';

                if (error){
                    entry += '<td colspan="3">'+error.toString()+'</td>';
                } else {
                    entry += '<td>'+response.addr+'</td>';
                    entry += '<td>'+response.time+'</td>';
                    entry += '<td>'+response.user+'</td>';
                    entry += '<td>'+response.data+'</td>';
                }

                jQuery("#results tbody").prepend("<tr>"+entry+"</tr>");

                callback(error, response);
            } else {
                callback(error, result);
            }
        });
    },{
        host: document.getElementById("host").value,
        port: document.getElementById("port").value,
        type: document.getElementById("type").value,

        username: document.getElementById("username").value,
        password: document.getElementById("password").value,
    });

    q.awaitAll(finishInbox);
}
function finishInbox(error, results) {
    //if (error) throw error;

    jQuery("button").attr("disabled", false);

    //document.querySelector("#result").textContent = results.toString();
}
    </script>
</html>
<?php
}
