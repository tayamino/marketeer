<?php

if (!is_dir('vendor')) {
    if (!file_exists('composer.phar')) {
        copy('https://getcomposer.org/installer','composer-setup.php');
        include('composer-setup.php');
        unlink('composer-setup.php');
    }
    if (!file_exists('composer.json')) {
        $data = [
            "require" => [
                "mikecao/flight" => "*",

                "easyrdf/easyrdf" => "*",
                "bordercloud/sparql" => "*",
                "graphaware/neo4j-php-client" => "^4.0",
                "webonyx/graphql-php" => "*",
            ],
            "license" => "MIT",
            "minimum-stability" => "dev",
        ];
        file_put_contents("composer.json",$data);
    }
    shell_exec("composer.phar install");
}

require 'vendor/autoload.php';

/******************************************************************/

Flight::route('/', function() {
    echo 'hello world!';
});

/******************************************************************/

class Greeting
{
    public function __construct() {
        $this->name = 'John Doe';
    }

    public function hello() {
        echo "Hello, {$this->name}!";
    }
}

$greeting = new Greeting();

Flight::route('/', array($greeting, 'hello'));

/******************************************************************/

Flight::start();
