<?php

$config = [
    "zen" => ["label" => "ZenDesk",
        "domain" => "datamake",
        "key" => "service@datamake.cn",
        "secret" => "EHtVbEQEPAwpswIKoLBIjrdu7MOFjXnTrsmZkzj4",
        "author" => "416158848274",
    ],
    "fresh" => ["label" => "FreshDesk",
        "domain" => "fraxelgames",
        "key" => "L894TBIUZFl1nVnp8b5",
        "secret" => "Azerty123@",
        "config" => 77000035213,
    ],
    "grid" => ["label" => "SendGrid",
        "token" => "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",
    ],
    "drill" => ["label" => "Mandrill",
        "token" => "lsdflksdklfjskldfsdf",
    ],
    "gun" => ["label" => "MailGun",
        "domain" => "xxxyyyzzz",
        "secret" => "xxxyyyzzz",
    ],
];

function formats($text,$data) {
    $listing = [
        "[-today-]" => date("m/d/Y", time()),
        "[-now-]" => date("h:i:s a", time()),
        "[-email-]" => $data['email'],
        "[-title-]" => $data['title'],
        "[-link-]" => $data['link'],
        "[-letters-]" => randString("abcdefghijklmnopqrstuvwxyz", 8, 15),
        "[-string-]" => randString("abcdefghijklmnopqrstuvwxyz0123456789", 8, 15),
        "[-number-]" => randString("0123456789", 7, 15),
        "[-md5-]" => md5(rand()),
    ];

    foreach ($listing as $search => $value) {
        $text = str_replace($search, $value, $text);
    }

    return $text;
}

//##########################################################################################

function send_zen ($api, $data) {
    $resp = request("https://{$api['domain']}.zendesk.com/api/v2/tickets", json_encode([
        "ticket" => [
            "status" => "new",
            "problem_id" => "",
            "group_id" => "",
            "assignee_id" => "",
            "priority"=>"normal",
            "tags" => [],
            "via" => [
                "channel" => "Web form"
            ],
            "followers" => [],
            "email_ccs" => [],
            "submitter_id" => 416158848274,
            "requester" => [
                "name" => $data["title"],
                "email" => $data["email"],
            ],
            "custom_fields" => [],
            "comment" => [
                "html_body" => $data["content"],
                "public" => true,
                "author_id" => $api["author"],
                "uploads" => []
            ],
            "subject" => $data["subject"]
        ]
    ]), [
        "Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}/token:{$api['secret']}");

    return $resp;
}

function send_fresh ($api, $data) {
    $resp = request("https://{$api['domain']}.freshdesk.com/api/v2/tickets/outbound_email", json_encode([
        "name" => $data["title"],
        "email" => $data["email"],
        "status" => 5,
        "subject" => $data["subject"],
        "description" => $data["content"],
        "priority" => 1,
        "email_config_id" => $api["config"],
    ]), [
        "Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}:{$api['secret']}");

    return $resp;
}

//##########################################################################################

function send_drill ($api, $data) {
    $resp = request("https://mandrillapp.com/api/1.0/messages/send.json", json_encode([
        "content" => [
            "to" => [
                [
                    "email" => $data["email"],
                    "name" => $data["title"],
                ],
            ],
            "subject" => $data["subject"],

            "html" => $data["content"],
            "text" => $data["content"],
        ],
        "key" => $api["token"],
    ]), [
        "Content-Type: application/json; charset=UTF-8",
    ], null);

    return $resp;
}

function send_grid ($api, $data) {
    $resp = request("https://api.sendgrid.com/v3/mail/send", json_encode([
        "personalizations" => [
            [
                "to" => [
                    "name" => $data["title"],
                    "email" => $data["email"],
                ],
                "subject" => $data["subject"],
            ],
        ],
        //"from" => [
        //    "email" => $data["subject"],
        //],
        "content" => [
            [
                "type" => "text/html",
                "value" => $data["content"],
            ],
        ],
    ]), [
        "Authorization: Bearer {$api['token']}",
        "Content-Type: application/json",
    ], null);

    return $resp;
}

function send_gun ($api, $data) {
    $resp = request("https://api.mailgun.net/v3/{$api['domain']}/messages", json_encode([
        "to" => [
            "name" => $data["title"],
            "email" => $data["email"],
        ],
        "subject" => $data["subject"],
    ]), [
        "Content-Type: application/json",
    ], "api:{$api['secret']}");

    return $resp;
}

//##########################################################################################

function send_mark ($api, $data) {
    $resp = request("https://api.postmarkapp.com/email", json_encode([
        "From" => $data["title"],
        "To" => $data["email"],
        "Subject" => $data["subject"],
        "MessageStream" => "outbound",
        "TextBody" => $data["content"],
        "HtmlBody" => $data["content"],
    ]), [
        "Accept: application/json",
        "Content-Type: application/json",
        "X-Postmark-Server-Token: {$api['token']}",
    ], null);

    return $resp;
}

function send_elastic ($api, $data) {
    $resp = request("https://api.elasticemail.com/v2/email/send" . http_build_query([
        "subject" => $data["subject"],
        //"from" => "",
        //"fromName" => "",
        //"sender" => "",
        //"senderName" => "",
        //"msgFrom" => "",
        //"msgFromName" => "",
        "to" => $data["email"],
        //"channel" => "",
        "bodyHtml" => "",
        "bodyText" => "",
        //"isTransactional" => false,
        //"trackOpens" => true,
        //"trackClicks" => true,
        "apikey" => $api["token"],
    ]), ([
    ]), [
        "Content-Type: multipart/form-data",
    ], null);

    return $resp;
}

//##########################################################################################

function is_email($input) {
  $email_pattern = "/^([a-zA-Z0-9\-\_\.]{1,})+@+([a-zA-Z0-9\-\_\.]{1,})+\.+([a-z]{2,4})$/i";
  if(preg_match($email_pattern, $input)) return TRUE;
}

function request ($url,$body,$head,$auth) {
    $resp = [];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");

    curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    if ($auth!=":" && $auth!=null)
        curl_setopt($ch, CURLOPT_USERPWD, $auth);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $resp["error"] = curl_error($ch);
    }
    curl_close($ch);

    $resp["result"] = json_decode($result,true);

    return $resp;
}

function stringInsert($str,$insertstr,$pos) {
    $str = substr($str, 0, $pos) . $insertstr . substr($str, $pos);
    return $str;
}

function randString($consonants, $min_length, $max_length) {
    $length=rand($min_length, $max_length);
    $password = "";
    for ($i = 0; $i < $length; $i++) {
            $password .= $consonants[(rand() % strlen($consonants))];
    }
    return $password;
}

//##########################################################################################

if ($_SERVER['REQUEST_METHOD']=='POST') {
    $data = json_decode(file_get_contents("php://input"),true);

    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    if(!is_email($data["email"])){
        echo("Incorrect Email");
        exit();
    }

    $data["title"] = preg_replace("/([^@]*).*/", "$1", $data['email']);

    $resp = [];

    $data["content"] = formats($data['letter'],$data);

    if (array_key_exists($data["router"],$config)) {
        $name = "send_{$data["router"]}";

        if (function_exists($name)) {
            $resp = call_user_func($name, $config[$data["router"]], $data);

            if (array_key_exists('result',$resp)) {
                if (array_key_exists('errors',$resp['result'])) {
                    $resp['error'] = $resp['result']['errors'];
                }
            }
        } else {
            $resp["error"] = "Undeclared function";
        }
    } else {
        $resp["error"] = "Missing configuration";
    }

    echo json_encode(array_merge($data,$resp));
} else {
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>GRINI</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/cosmo/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap core CSS -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <!-- Custom styles for this template -->
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <!--<script src="resources/js/json-to-PHP.js" type="text/javascript"></script> -->
    </head>
    <body class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <hr>
                    <input type="text" class="form-control col-10" placeholder="Subject" id="subject" value="test">
                    <hr>
                    <textarea id="letter"  rows="3" class="form-control" placeholder="Message">[-link-]</textarea>
                    <hr>
                    <input type="text" id="link" class="form-control" placeholder="Web Link" value="#">
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="router">Provider</label>
                            <select id="router" name="router" class="form-control col-12">
                                <option value="test">Please select one</option>
<?php foreach ($config as $name => $conf) { ?>
                                <option value="<?php echo $name ?>"><?php echo $conf['label'] ?></option>
<?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="limit">Group Limit</label>
                            <label for="sleep">Sleeping</label>
                            <input type="text" id="limit" class="form-control" placeholder="Limit" value="5">
                            <input type="text" id="sleep" class="form-control" placeholder="Sleep" value="2.5">
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-default btn-sm" onclick="startSending()" id="envoyerbtn">START</button>
                            <br>
                            <span id="compteur"></span>
                        </div>
                    </div>
                    <hr>
                    <textarea id="emailList" rows="7" class="form-control" placeholder="Emails"></textarea>
                </div>
                <div class="col-md-6">
                    <hr>
                    <div id="progress" class="col-lg-16"></div>
                </div>
            </div>
        </div>
    </body>
    <style>
.clignote
{
  animation: Test 1s infinite;
  color:blue;
}
.success
{
  color:green;
  font-weight: bold
}
.error
{
  color:red;
  font-weight: bold
}
.devider
{
 height: 20px;
 width: 100%;
}
.normal
{
  color:black;
  
}
div#progress {
    border: solid 1px gray;
}
@keyframes Test{
    0%{opacity: 1;}
    50%{opacity: 0;}
    100%{opacity: 1;}
}
label[for="limit"],input#limit{
    width: 50%;
    float: left;
}
label[for="sleep"],input#sleep{
    width: 50%;
    float: right;
}
    </style>
    <script src="https://d3js.org/d3-collection.v1.min.js"></script>
    <script src="https://d3js.org/d3-dispatch.v1.min.js"></script>
    <script src="https://d3js.org/d3-dsv.v1.min.js"></script>
    <script src="https://d3js.org/d3-request.v1.min.js"></script>
    <script src="https://d3js.org/d3-queue.v3.min.js"></script>
    <script>
var txt = document.getElementById("emailList");

for (i=0 ; i<100 ; i++) {
    txt.value += "test."+i.toString()+"@hotmail.com\n";
}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

var multi = 1;
var queue = null;
var waits = null;
var clock = null;
var index = 0;
var limit = 1;
var total = 0;
var sleep = 1000;

function highlight(name) {
    var obj = document.getElementById(name);

    if (obj.value.trim().length==0) {
        alert("Empty field : "+name);

        obj.focus();

        return false;
    }

    return true;
}
function startSending() {
    waits = [];

    var source = document.getElementById("emailList").value.split("\n");

    for (i=0 ; i<source.length ; i++) {
        value = source[i].trim();

        if (value!="") {
            waits.push(value);
        }
    }
    
    jQuery("#envoyerbtn").attr("disabled", "disabled");

    total = waits.length;

    jQuery("#progress").empty();
    
    limit = parseInt(document.getElementById("limit").value);

    sleep = parseFloat(document.getElementById("sleep").value) * 1000;

    looperSend();
}
function handleSend(error, results) {
    //if (error) throw error;

    if (waits.length) {
        //*
        jQuery("#compteur").text("Sleeping for "+sleep+" ms");

        clock = setTimeout(function () { clearTimeout(clock); looperSend(); },sleep);

        jQuery("#envoyerbtn").attr("class","btn btn-default btn-sm");
        //*/
    } else {
        finishSend();
    };
}
function finishSend() {
    jQuery("#envoyerbtn").attr("disabled", true);
    jQuery("#compteur").text("DONE");
    jQuery("#envoyerbtn").attr("class","btn btn-default btn-sm");
}
function looperSend() {
    queue = d3.queue(multi);

    var offset = index * limit;
    
    jQuery("#envoyerbtn").attr("class","btn btn-success btn-sm");

    var emails = waits.slice(0,limit);
    
    waits = waits.slice(limit);
    
    console.log("loop : ",offset,index,limit,total);
    
    jQuery("#compteur").text(offset+" / "+total);

    var subje=document.getElementById("subject").value;
    var letter=document.getElementById('letter').value;
    var link=document.getElementById('link').value;

    var router=document.getElementById("router").value;

    if (!highlight('emailList') || !highlight('link') || !highlight('subject') || !highlight('letter') || !highlight('router')) {
        return;
    }

    index += 1;
    
    for (i=0 ; i<emails.length ; i++) {
        var recipient = emails[i];

        if(emailReg.test(recipient))
            queue.defer(function (data, callback) {
                d3.request("?")
                  .header("Content-Type", "application/json")
                  .post(JSON.stringify(data), function(error, result) {
                    var response = result;
                    if (result) {
                        response = JSON.parse(result.response);

                        error = error || response.error;

                        jQuery("#progress").append('<div class="col-lg-3">' + (response.index+offset+1).toString() + '/' + (total) + '</div>');

                        message = response.email;

                        if (error) {
                            message += '<span class="label label-warning">'+JSON.stringify(error)+'</span>';
                        }

                        jQuery("#progress").append('<div class="col-lg-6">' + message + '</div>');
    
                        if (error){
                            jQuery("#progress").append('<div class="col-lg-1"><span class="label label-danger">failed</span></div>');
                        } else {
                            jQuery("#progress").append('<div class="col-lg-1"><span class="label label-success">7wi Listat :)</span></div>');
                        }

                        jQuery("#progress").append('<br>');

                        window.scrollTo(0,document.body.scrollHeight);
                    }
                    error = null;
                    callback(error, response);
                });
            },{
                index:i,
                total:emails.length,
                email:recipient,
                link:link,
                subject:subje,
                letter:letter,
                router:router,
            });
    }
    
    queue.awaitAll(handleSend);
}
    </script>
</html>
<?php
}

