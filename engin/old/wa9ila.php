<?php

$config = [
    "twilio" => [
        "label" => "Twilio API",

        "key" => "AC9793e1395fc410f66946e27d9e442a3a",
        "secret" => "1fa38de1a247bc48a00207098107e19b",
    ],
    "plivo" => [
        "label" => "Plivo SMS",

        "key" => "MAZTI5MWUZZDY5NTCYYJ",
        "secret" => "YzAyM2ExNjdiOTA0YjA2NTdiNzhmOTkyOTBmZWIx",
    ],
    "nexmo" => [
        "label" => "Nexmo SMS",

        "key" => "xxxxxxxxxxxxxxxxxxxxxxxx",
        "secret" => "yyyyyyyyyyyyyyyyyyyyyyyy",
    ],
    "edges" => [
        "label" => "SMSedge",

        "key" => "xxxxxxxxxxxxxxxxxxxxxxxx",
    ],
];

function formats($text,$data) {
    $listing = [
        "[-today-]" => date("m/d/Y", time()),
        "[-now-]" => date("h:i:s a", time()),
        "[-phone-]" => $data['phone'],
        "[-title-]" => $data['title'],
        "[-letters-]" => randString("abcdefghijklmnopqrstuvwxyz", 8, 15),
        "[-string-]" => randString("abcdefghijklmnopqrstuvwxyz0123456789", 8, 15),
        "[-number-]" => randString("0123456789", 7, 15),
        "[-md5-]" => md5(rand()),
    ];

    foreach ($listing as $search => $value) {
        $text = str_replace($search, $value, $text);
    }

    return $text;
}

//##########################################################################################

function send_twilio ($api, $data) {
    $resp = request("https://api.twilio.com/2010-04-01/Accounts/{$api['key']}/Messages.json", http_build_query([
        "Body" => "{$data['content']}",
        "From" => "{$data['subject']}",
        "To"   => "{$data['phone']}",
    ]),[
        //"Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}:{$api['secret']}");

    return $resp;
}

//******************************************************************************************

function cash_twilio ($api) {
    $resp = request("https://api.twilio.com/2010-04-01/Accounts/{$api['key']}/Balance.json", null, [
        //"Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}:{$api['secret']}");

    return $resp;

    $cash = 0;

    $cash = floatval($resp['balance']);

    return [$cash,$resp['currency']];
}

//##########################################################################################

function send_nexmo ($api, $data) {
    $resp = request("https://rest.nexmo.com/sms/json", http_build_query([
        "from" => "{$data['subject']}",
        "text" => "{$data['content']}",
        "to"   => "{$data['phone']}",
        "api_key" => "{$api['key']}",
        "api_secret" => "{$api['secret']}",
    ]), [
        //"Content-Type: application/json; charset=UTF-8",
    ], null);

    return $resp;
}

//******************************************************************************************

function cash_nexmo ($api) {
    $resp = request("https://rest.nexmo.com/account/get-balance?".http_build_query([
        "api_key" => "{$api['key']}",
        "api_secret" => "{$api['secret']}",
    ]), null, [
        //"Content-Type: application/json; charset=UTF-8",
    ], null);

    return [$resp,"EUR"];
}

//##########################################################################################

function send_plivo ($api, $data) {
    $resp = request("https://api.plivo.com/v1/Account/{$api['key']}/Message/", json_encode([
        "src" => $data["subject"],
        "dst" => $data["phone"],
        //"url" => "http://webhook.site/c7a0b423-8d1f-46fe-9a81-1dfa8254bdaf"
        "text" => $data["content"],
    ]), [
        "Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}:{$api['secret']}");

    return $resp;
}

//******************************************************************************************

function cash_plivo ($api) {
    $resp = request("https://api.plivo.com/v1/Account/", null, [
        "Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}:{$api['secret']}");

    $cash = floatval($resp['balance']);

    return [$cash,'EUR'];
}

//##########################################################################################

function send_edges ($api, $data) {
    $resp = request("https://api.smsedge.com/v1/sms/send-single/?".http_build_query([
        "from"          => $data["subject"],
        "to"            => $data["phone"],
        "text"          => $data["content"],

        "api_token"     => $api['key'],

        "shorten_url"   => "0",
        "smart_routing" => "false",
        "transactional" => "0",
    ]), null, [
        //"Content-Type: application/json; charset=UTF-8",
    ], null);

    return $resp;
}

//******************************************************************************************

function cash_edges ($api) {
    $resp = request("https://api.smsedge.com/v1/sms/send-single/?".http_build_query([
        "from"          => $data["subject"],
        "to"            => $data["phone"],
        "text"          => $data["content"],

        "api_token"     => $api['secret'],

        "shorten_url"   => "0",
        "smart_routing" => "false",
        "transactional" => "0",
    ]), null, [
        //"Content-Type: application/json; charset=UTF-8",
    ], null);
    
    $cash = 0;

    if (array_key_exists("data",$resp)) {
        if (array_key_exists("cost",$resp['data'])) {
            $cash = $resp['data']['cost'];
        }
    }

    return [$cash,'EUR'];
}

//##########################################################################################

function send_msg91 ($api, $data) {
    $resp = request("https://api.smsedge.com/v1/sms/send-single/?".http_build_query([
        "from"          => $data["subject"],
        "to"            => $data["phone"],
        "text"          => $data["content"],

        "api_token"     => $api['secret'],

        "shorten_url"   => "0",
        "smart_routing" => "false",
        "transactional" => "0",
    ]), null, [
        //"Content-Type: application/json; charset=UTF-8",
    ], null);

    return $resp;
}

//******************************************************************************************

function cash_msg91 ($api) {
    $resp = request("https://api.smsedge.com/v1/sms/send-single/?".http_build_query([
        "from"          => $data["subject"],
        "to"            => $data["phone"],
        "text"          => $data["content"],

        "api_token"     => $api['secret'],

        "shorten_url"   => "0",
        "smart_routing" => "false",
        "transactional" => "0",
    ]), null, [
        //"Content-Type: application/json; charset=UTF-8",
    ], null);
    
    $cash = 0;

    if (array_key_exists("data",$resp)) {
        if (array_key_exists("cost",$resp['data'])) {
            $cash = $resp['data']['cost'];
        }
    }

    return [$cash,'EUR'];
}

//##########################################################################################

function is_phone($input) {
  return true;
  $phone_pattern = "/^([a-zA-Z0-9\-\_\.]{1,})+@+([a-zA-Z0-9\-\_\.]{1,})+\.+([a-z]{2,4})$/i";
  if(preg_match($phone_pattern, $input)) return TRUE;
}

function request ($url,$body,$head,$auth=null,$json=true) {
    $resp = [];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if ($body!=null) {
        if ($body) {
            curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        }
        curl_setopt($ch, CURLOPT_POST, 1);
    }
    curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");

    curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    if ($auth!=":" && $auth!=null)
        curl_setopt($ch, CURLOPT_USERPWD, $auth);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $resp["error"] = curl_error($ch);
    }
    curl_close($ch);

    $resp["result"] = json_decode($result);

    return $resp;
}

function stringInsert($str,$insertstr,$pos) {
    $str = substr($str, 0, $pos) . $insertstr . substr($str, $pos);
    return $str;
}

function randString($consonants, $min_length, $max_length) {
    $length=rand($min_length, $max_length);
    $password = "";
    for ($i = 0; $i < $length; $i++) {
            $password .= $consonants[(rand() % strlen($consonants))];
    }
    return $password;
}

//##########################################################################################

if (isset($_GET['balance'])) {
    if (array_key_exists($_GET['balance'],$config)) {
        $name = "cash_{$_GET['balance']}";

        if (function_exists($name)) {
            $resp = call_user_func($name, $config[$_GET['balance']]);
        } else {
            $resp["error"] = "Undeclared function";
        }
    } else {
        $resp["error"] = "Missing configuration";
    }

    echo json_encode($resp);
} else if ($_SERVER['REQUEST_METHOD']=='POST') {
    $data = json_decode(file_get_contents("php://input"),true);

    // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
    if(!is_phone($data["phone"])){
        echo("Incorrect phone");
        exit();
    }

    $data["title"] = preg_replace("/([^@]*).*/", "$1", $data['phone']);

    $resp = [];

    $data["content"] = formats($data['letter'],$data);

    if (array_key_exists($data["router"],$config)) {
        $name = "send_{$data["router"]}";

        if (function_exists($name)) {
            $resp = call_user_func($name, $config[$data["router"]], $data);
        } else {
            $resp["error"] = "Undeclared function";
        }
    } else {
        $resp["error"] = "Missing configuration";
    }

    echo json_encode(array_merge($data,$resp));
} else {
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>GRINI</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/cosmo/bootstrap.min.css" rel="stylesheet">
        <!-- Bootstrap core CSS -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <!-- Custom styles for this template -->
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <!--<script src="resources/js/json-to-PHP.js" type="text/javascript"></script> -->
    </head>
    <body class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <hr>
                    <input type="text" class="form-control col-10" placeholder="Sender Name" id="subject" value="">
                    <hr>
                    <textarea id="letter"  rows="3" class="form-control" placeholder="Message"></textarea>
                </div>
                <div class="col-md-6">
                    <hr>
                    <textarea id="phoneList" rows="3" class="form-control" placeholder="Phone Numbers"></textarea>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <select id="router" name="router" class="form-control col-12">
                                <option>Sending Method</option>
<?php foreach ($config as $name => $conf) { ?>
                                <option value="<?php echo $name ?>"><?php echo $conf['label'] ?></option>
<?php } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-default btn-sm" onclick="checkBalance()" id="balancebtn">CHECK</button>
                        </div>
                        <div class="col-md-2">
                          <span class="label label-warning">Balance :</span>
                          <span id="balance" class="label label-success">0 EUR</span>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-default btn-sm" onclick="startSending()" id="envoyerbtn">START</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <hr>
                    <div id="progress" class="col-lg-16"></div>
                </div>
            </div>
        </div>
    </body>
    <style>
.clignote
{
  animation: Test 1s infinite;
  color:blue;
}
.success
{
  color:green;
  font-weight: bold
}
.error
{
  color:red;
  font-weight: bold
}
.devider
{
 height: 20px;
 width: 100%;
}
.normal
{
  color:black;

}
@keyframes Test{
    0%{opacity: 1;}
    50%{opacity: 0;}
    100%{opacity: 1;}
}
    </style>
    <script src="https://d3js.org/d3-collection.v1.min.js"></script>
    <script src="https://d3js.org/d3-dispatch.v1.min.js"></script>
    <script src="https://d3js.org/d3-dsv.v1.min.js"></script>
    <script src="https://d3js.org/d3-request.v1.min.js"></script>
    <script src="https://d3js.org/d3-queue.v3.min.js"></script>
    <script>
var CONCURRENT = 4;

var phoneReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function highlight(name) {
    var obj = document.getElementById(name);

    if (obj.value.trim().length==0) {
        alert("Empty field : "+name);

        obj.focus();

        return false;
    }

    return true;
}
function checkBalance() {
    var router=document.getElementById("router").value;

    if (!highlight('router')) {
        return;
    }

    jQuery("#balancebtn").attr("disabled", true);

    d3.request("?balance="+router)
      .header("Content-Type", "application/json")
      .post(JSON.stringify([]), function(error, result) {
        if (result) {
            var response = JSON.parse(result.response);

            document.getElementById("balance").innerHTML = response[0]+" "+response[1];
        } else {
            //callback(error, result);
        }

        jQuery("#balancebtn").attr("disabled", false);

        window.scrollTo(0,document.body.scrollHeight);
    });
}
function startSending() {
    var q = d3.queue(CONCURRENT);

    var source = document.getElementById("phoneList").value.split("\n"), phones = [];

    for (i=0 ; i<source.length ; i++) {
        value = source[i].trim();

        if (value!="") {
            phones.push(value);
        }
    }

    jQuery("#progress").empty();

    var subje=document.getElementById("subject").value;
    var letter=document.getElementById('letter').value;

    var router=document.getElementById("router").value;

    if (!highlight('phoneList') || !highlight('subject') || !highlight('letter') || !highlight('router')) {
        return;
    }

    jQuery("#envoyerbtn").attr("disabled", true);

    for (i=0 ; i<phones.length ; i++) {
        var recipient = phones[i]

        if(phoneReg.test(recipient) || true)
            q.defer(function (data, callback) {
                d3.request("?")
                  .header("Content-Type", "application/json")
                  .post(JSON.stringify(data), function(error, result) {
                    if (result) {
                        var response = JSON.parse(result.response);

                        jQuery("#progress").append('<div class="col-lg-3">' + (response.index+1).toString() + '/' + response.total + '</div><div class="col-lg-6">' + response.phone + '</div>');

                        if (error){
                            jQuery("#progress").append('<div class="col-lg-1"><span class="label label-default">failed</span></div>');
                        } else {
                            jQuery("#progress").append('<div class="col-lg-1"><span class="label label-success">7wi Listat :)</span></div>');
                        }

                        jQuery("#progress").append('<br>');

                        window.scrollTo(0,document.body.scrollHeight);

                        callback(error, response);
                    } else {
                        callback(error, result);
                    }
                });
            },{
                index:i,
                total:phones.length,
                phone:recipient,
                subject:subje,
                letter:letter,
                router:router,
            });
    }

    q.awaitAll(finishSending);
}
function finishSending(error, results) {
    //if (error) throw error;

    jQuery("#envoyerbtn").attr("disabled", false);

    //document.querySelector("#result").textContent = results.toString();
}
    </script>
</html>
<?php
}
