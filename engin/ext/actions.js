var mockup_code,mockup_temp=null;
function mockup_page (info,tab) {
    var source = '<!DOCTYPE HTML>' + '\n';
    source += document.documentElement.outerHTML;

    localStorage['tempMockup'] = JSON.stringify({
        name: tab.title,
        link: tab.url,
        html: source,
    });

    chrome.tabs.create({
        url: chrome.extension.getURL('/model/make.html')
    });
}
function mockup_link (info,tab) {
    mockup_temp = {
        name: tab.title,
        link: tab.url,
    };
    const executing = browser.tabs.executeScript({
        code: mockup_code,
    });
    executing.then(mockup_data, mockup_fail);
}
function mockup_data(source) {
    mockup_temp.html = source;

    localStorage['tempMockup'] = JSON.stringify(mockup_temp);

    chrome.tabs.create({
        url: chrome.extension.getURL('/model/make.html')
    });
    mockup_temp = null;
}
function mockup_fail(error) {
    console.log(`Error: ${error}`);
}
mockup_code = "var source = '<!DOCTYPE HTML>' + '\\n';\n";
mockup_code += "source += document.documentElement.outerHTML;";

chrome.contextMenus.create({
    contexts:["page","frame"],
    title: "Mockup this link",
    onclick: mockup_link,
});
/*
chrome.contextMenus.create({
    onclick: function (info,tab) {
      //console.log("Word " + info.selectionText + " was clicked.");
      chrome.tabs.create({
        url: "http://www.google.com/search?q=" + info.selectionText
      });
    },
    contexts:["link"],
    title: "Mockup this link",
});

chrome.contextMenus.create({
    onclick: function (info,tab) {
      //console.log("Word " + info.selectionText + " was clicked.");
      chrome.tabs.create({
        url: "http://www.google.com/search?q=" + info.selectionText
      });
    },
    contexts:["frame"],
    title: "Mockup this frame",
});

chrome.contextMenus.create({
    onclick: function (info,tab) {
      //console.log("Word " + info.selectionText + " was clicked.");
      chrome.tabs.create({
        url: "http://www.google.com/search?q=" + info.selectionText
      });
    },
    contexts:["page"],
    title: "Mockup this page",
});

chrome.contextMenus.create({
    onclick: function (info,tab) {
      //console.log("Word " + info.selectionText + " was clicked.");
      chrome.tabs.create({
        url: "http://www.google.com/search?q=" + info.selectionText
      });
    },
    contexts:["launcher"],
    title: "Mockup this launcher",
});

chrome.contextMenus.create({
    onclick: function (info,tab) {
      //console.log("Word " + info.selectionText + " was clicked.");
      chrome.tabs.create({
        url: "http://www.google.com/search?q=" + info.selectionText
      });
    },
    contexts:["action"],
    title: "Mockup this action",
});
//*/

