<?php

class Cacheable {
    public function __construct ($folder,$method='md5') {
        $this->folder = $folder;
        $this->method = $method;
    }
    public function key ($value) {
        switch ($this->method) {
            case 'md5':
                return md5($value);
                break;
        }
        return $value;
    }
    public function get ($alias) {
        
    }
    public function set ($alias,$value) {
        
    }
    public function all () {
        
    }
}

/******************************************************************************/

global $actes; $actes=[];

function register ($alias,$handler,$format=null) {
    global $actes;
    if (!array_key_exists($alias,$actes)) {
        $actes[$alias] = [
            'name' => $alias,
            'func' => $handler,
            'type' => $format,
        ];
    }
}

/******************************************************************************/

global $fa_ico; $fa_ico=[];

function fa_icon ($alias,$value=null) {
    global $fa_ico;
    if ($value) {
        $fa_ico[$alias] = $value;
        
        return $value;
    } else {
        if (array_key_exists($alias,$fa_ico)) {
            return $fa_ico[$alias];
        }
    }
    return 'default';
}

/******************************************************************************/

fa_icon('css',  'css3');
fa_icon('html', 'html5');
fa_icon('js',   'code');

fa_icon('php', 'coffee');
fa_icon('py',  'coffee');

fa_icon('ttf',   'font');
fa_icon('woff2', 'font');

foreach ([
    'file-text'      => ['rtf','txt'],
    'file-picture-o' => ['ico','svg'],
    'file-photo-o'   => ['jpg','jpeg','gif','png'],
    'file-audio-o'   => ['rtf','txt'],
    'file-music'     => ['wav','aac','ogg','m4a','mp3','opus','wma'],
    'file-video-o'   => ['avi','mkv','mp4','wmv'],
    'file-archive-o' => ['rar','tar','tgz','xz','zip'],
] as $key => $lst) {
    foreach ($lst as $ico) {
        fa_icon($key,$ico);
    }
}

fa_icon('doc', 'file-word-o');
fa_icon('xls', 'file-excel-o');
fa_icon('ppt', 'file-powerpoint-o');

fa_icon('pdf',    'file-pdf-o');

fa_icon('csv',    'grid');
fa_icon('json',   'database');
fa_icon('jsonld', 'share-alt');

