<?php

function is_email ($input) {
    $email_pattern = "/^([a-zA-Z0-9\-\_\.]{1,})+@+([a-zA-Z0-9\-\_\.]{1,})+\.+([a-z]{2,4})$/i";
    if(preg_match($email_pattern, $input)) return TRUE;
}

/******************************************************************************/

function is_phone ($input) {
    return true;
    $phone_pattern = "/^([a-zA-Z0-9\-\_\.]{1,})+@+([a-zA-Z0-9\-\_\.]{1,})+\.+([a-z]{2,4})$/i";
    if(preg_match($phone_pattern, $input)) return TRUE;
}

//##########################################################################################

function randString($consonants, $min_length, $max_length) {
    $length=rand($min_length, $max_length);
    $password = "";
    for ($i = 0; $i < $length; $i++) {
            $password .= $consonants[(rand() % strlen($consonants))];
    }
    return $password;
}

//##########################################################################################

function formats($text,$data) {
    return $text;

    $listing = [
        "[-today-]" => date("m/d/Y", time()),
        "[-now-]" => date("h:i:s a", time()),
        "[-email-]" => $data['email'],
        "[-title-]" => $data['title'],
        //"[-link-]" => $data['link'],
        "[-letters-]" => randString("abcdefghijklmnopqrstuvwxyz", 8, 15),
        "[-string-]" => randString("abcdefghijklmnopqrstuvwxyz0123456789", 8, 15),
        "[-number-]" => randString("0123456789", 7, 15),
        "[-md5-]" => md5(rand()),
    ];

    foreach ($listing as $search => $value) {
        $text = str_replace($search, $value, $text);
    }

    $text = str_replace("\n", "", $text);

    return $text;
}
