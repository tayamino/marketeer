<?php

function listing ($target,$format=null) {
    $result = [];

    if (is_dir($target)) {
        foreach ( scandir($target) as $entry ) {
            //if (!in_array($entry,['.'])) {
            if (!in_array($entry,['.','..'])) {
                $status = true;

                switch ($format) {
                    case 'folder':
                        $status = is_dir($target);
                        break;
                    case 'file':
                        $status = is_file($target);
                        break;
                }
                
                if ($status) {
                    $result[] = $entry;
                }
            }
        }
    }

    return $result;
}

//##########################################################################################

function request ($url,$body,$head,$auth) {
    $resp = [];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");

    curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    if ($auth!=":" && $auth!=null)
        curl_setopt($ch, CURLOPT_USERPWD, $auth);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $resp["error"] = curl_error($ch);
    }
    curl_close($ch);

    $resp["result"] = json_decode($result,true);

    return $resp;
}

