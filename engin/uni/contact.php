<?php

register('info',function () {
    $data = [];

    foreach ( listing('user',null) as $entry ) {
        $item = [
            'name' => $entry,
            'path' => realpath("user/{$entry}"),
        ];
        $item['type'] = strtolower(pathinfo($item['path'], PATHINFO_EXTENSION));
        $item['uuid'] = substr($item['name'],0,strlen($item['name'])-strlen($item['type'])-1);

        switch ($item['type']) {
            case 'vcard':
            case 'vcf':
                if (isset($_GET['list']) and in_array($entry,$_GET['list'])) {
                    $item['list'] = [];
                    
                    $parser = VCardParser::parseFromFile($item['path']);

                    foreach($parser as $vcard) {
                        $objet = [
                            'title' => "{$vcard->firstname} {$vcard->lastname}",
                            //'birth' => $vcard->birthday->format('Y-m-d'),
                            'email' => [],
                            'phone' => $vcard->phoneNumber,
                            'where' => $vcard->address,
                            'doing' => $vcard->role,
                        ];
                        /*
                        foreach ($vcard->email as $value) {
                            $objet['email'][] = $value;
                        }
                        foreach ($vcard->phoneNumber as $value) {
                            $objet['phone'][] = $value;
                        }
                        //*/
                        $item['list'][] = $objet;
                    }
                }
                $item['icon'] = 'id-card';
                break;
            case 'json':
            case 'jsonld':
                $item['data'] = json_decode(file_get_contents($item['path']));
                
                if (isset($_GET['list[]'])) {
                    $item['list'] = $item['data'];
                } else {
                    $item['list'] = $item['data'];
                }
                $item['icon'] = 'database';
                break;
            case 'csv':
            case 'tsv':
                $item['data'] = explode('\n',file_get_contents($item['path']));
                $item['meta'] = null;
                
                if (isset($_GET['list[]'])) {
                    $item['list'] = [];

                    foreach ($item['data'] as $line) {
                        $attr = explode(',',$line);

                        if (isset($item['meta'])) {
                            $item['meta'] = $attr;
                        } else {
                            $data = [];

                            for ($i=0 ; i<sizeof($attr) ; $i++) {
                                $data[$item['meta'][$i]] = $attr[$i];
                            }
                        }

                        $item['list'][] = $data;
                    }
                } else {
                    $item['list'] = [];
                }
                $item['icon'] = 'table';
                break;
            default:
                $item['text'] = file_get_contents($item['path']);
                $item['icon'] = 'file';
                break;
        }
        
        //print_r($link);die(1);

        $data[] = $item;
    }

    return $data;
},'json');

