<?php

libxml_use_internal_errors(true);

function bounce_ispdb ($domain) {
    $path = "http/autoconfig/{$domain}";

    $resp = null;

    if (file_exists($path)) {
        $resp = file_get_contents($path);
    } else {
        $resp = file_get_contents("https://autoconfig.thunderbird.net/v1.1/{$domain}");

        file_put_contents($path,$resp);
    }

    $data = simplexml_load_string($resp);

    if ($data === false) {
        echo "Failed loading XML: ";
        foreach(libxml_get_errors() as $error) {
            echo "<br>", $error->message;
        }
    } else {
        $resp = [];

        foreach ($data->emailProvider->incomingServer as $target) {
            $resp[$target->attributes()->type->__toString()] = [
                'host' => $target->hostname->__toString(),
                'port' => $target->port->__toString(),
                'type' => $target->socketType->__toString(),
            ];
        }

        foreach ($data->emailProvider->outgoingServer as $target) {
            $resp[$target->attributes()->type->__toString()] = [
                'host' => $target->hostname->__toString(),
                'port' => $target->port->__toString(),
                'type' => $target->socketType->__toString(),
            ];
        }

        //$resp = $data['emailProvider'];
    }

    return $resp;
}

function bounce_crawl ($address) {
    $path = "http/emailcrawlr/{$address}";

    $resp = [];

    if (file_exists($path)) {
        $resp = file_get_contents($path);
    } else {
        $keys = [
            'xxxxxxxxxx',
            'yyyyyyyyyy',
        ];

        for ($i=0 ; $i<sizeof($keys) ; $i++) {
            $resp = request("https://api.emailcrawlr.com/v2/email?email={$address}", null, [
                "x-api-key: "
            ],null);

            if ($resp!=null) {
                break;
            }
        }

        file_put_contents($path,$resp);
    }

    return $resp;
}

register('look',function () {
    $resp = [];

    if (isset($_GET['mail'])) {
        $resp['addr'] = $_GET['mail'];

        $resp['hash'] = md5($resp['addr']);
    }

    $part = explode('@',$resp['addr'],2);

    if (sizeof($part)==2) {
        $resp['name'] = $part[0];
        $resp['host'] = $part[1];
    }

    $resp['fqdn'] = bounce_ispdb($resp['host']);

    $resp['info'] = bounce_crawl($resp['addr']);

    return $resp;
},'json');
