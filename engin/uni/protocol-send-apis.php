<?php

global $cfgAPIs = [
    "zen" => ["label" => "ZenDesk",
        "domain" => "datamake",
        "key" => "service@datamake.cn",
        "secret" => "EHtVbEQEPAwpswIKoLBIjrdu7MOFjXnTrsmZkzj4",
        "author" => "416158848274",
    ],
    "fresh" => ["label" => "FreshDesk",
        "domain" => "fraxelgames",
        "key" => "L894TBIUZFl1nVnp8b5",
        "secret" => "Azerty123@",
        "config" => 77000035213,
    ],
    "grid" => ["label" => "SendGrid",
        "token" => "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",
    ],
    "drill" => ["label" => "Mandrill",
        "token" => "lsdflksdklfjskldfsdf",
    ],
    "gun" => ["label" => "MailGun",
        "domain" => "xxxyyyzzz",
        "secret" => "xxxyyyzzz",
    ],
];

function formats($text,$data) {
    $listing = [
        "[-today-]" => date("m/d/Y", time()),
        "[-now-]" => date("h:i:s a", time()),
        "[-email-]" => $data['email'],
        "[-title-]" => $data['title'],
        "[-link-]" => $data['link'],
        "[-letters-]" => randString("abcdefghijklmnopqrstuvwxyz", 8, 15),
        "[-string-]" => randString("abcdefghijklmnopqrstuvwxyz0123456789", 8, 15),
        "[-number-]" => randString("0123456789", 7, 15),
        "[-md5-]" => md5(rand()),
    ];

    foreach ($listing as $search => $value) {
        $text = str_replace($search, $value, $text);
    }

    return $text;
}

//##########################################################################################

function send_zen ($api, $data) {
    $resp = request("https://{$api['domain']}.zendesk.com/api/v2/tickets", json_encode([
        "ticket" => [
            "status" => "new",
            "problem_id" => "",
            "group_id" => "",
            "assignee_id" => "",
            "priority"=>"normal",
            "tags" => [],
            "via" => [
                "channel" => "Web form"
            ],
            "followers" => [],
            "email_ccs" => [],
            "submitter_id" => 416158848274,
            "requester" => [
                "name" => $data["title"],
                "email" => $data["email"],
            ],
            "custom_fields" => [],
            "comment" => [
                "html_body" => $data["content"],
                "public" => true,
                "author_id" => $api["author"],
                "uploads" => []
            ],
            "subject" => $data["subject"]
        ]
    ]), [
        "Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}/token:{$api['secret']}");

    return $resp;
}

function send_fresh ($api, $data) {
    $resp = request("https://{$api['domain']}.freshdesk.com/api/v2/tickets/outbound_email", json_encode([
        "name" => $data["title"],
        "email" => $data["email"],
        "status" => 5,
        "subject" => $data["subject"],
        "description" => $data["content"],
        "priority" => 1,
        "email_config_id" => $api["config"],
    ]), [
        "Content-Type: application/json; charset=UTF-8",
    ], "{$api['key']}:{$api['secret']}");

    return $resp;
}

//##########################################################################################

function send_drill ($api, $data) {
    $resp = request("https://mandrillapp.com/api/1.0/messages/send.json", json_encode([
        "content" => [
            "to" => [
                [
                    "email" => $data["email"],
                    "name" => $data["title"],
                ],
            ],
            "subject" => $data["subject"],

            "html" => $data["content"],
            "text" => $data["content"],
        ],
        "key" => $api["token"],
    ]), [
        "Content-Type: application/json; charset=UTF-8",
    ], null);

    return $resp;
}

function send_grid ($api, $data) {
    $resp = request("https://api.sendgrid.com/v3/mail/send", json_encode([
        "personalizations" => [
            [
                "to" => [
                    "name" => $data["title"],
                    "email" => $data["email"],
                ],
                "subject" => $data["subject"],
            ],
        ],
        //"from" => [
        //    "email" => $data["subject"],
        //],
        "content" => [
            [
                "type" => "text/html",
                "value" => $data["content"],
            ],
        ],
    ]), [
        "Authorization: Bearer {$api['token']}",
        "Content-Type: application/json",
    ], null);

    return $resp;
}

function send_gun ($api, $data) {
    $resp = request("https://api.mailgun.net/v3/{$api['domain']}/messages", json_encode([
        "to" => [
            "name" => $data["title"],
            "email" => $data["email"],
        ],
        "subject" => $data["subject"],
    ]), [
        "Content-Type: application/json",
    ], "api:{$api['secret']}");

    return $resp;
}

//##########################################################################################

function send_mark ($api, $data) {
    $resp = request("https://api.postmarkapp.com/email", json_encode([
        "From" => $data["title"],
        "To" => $data["email"],
        "Subject" => $data["subject"],
        "MessageStream" => "outbound",
        "TextBody" => $data["content"],
        "HtmlBody" => $data["content"],
    ]), [
        "Accept: application/json",
        "Content-Type: application/json",
        "X-Postmark-Server-Token: {$api['token']}",
    ], null);

    return $resp;
}

function send_elastic ($api, $data) {
    $resp = request("https://api.elasticemail.com/v2/email/send" . http_build_query([
        "subject" => $data["subject"],
        //"from" => "",
        //"fromName" => "",
        //"sender" => "",
        //"senderName" => "",
        //"msgFrom" => "",
        //"msgFromName" => "",
        "to" => $data["email"],
        //"channel" => "",
        "bodyHtml" => "",
        "bodyText" => "",
        //"isTransactional" => false,
        //"trackOpens" => true,
        //"trackClicks" => true,
        "apikey" => $api["token"],
    ]), ([
    ]), [
        "Content-Type: multipart/form-data",
    ], null);

    return $resp;
}

//##########################################################################################

