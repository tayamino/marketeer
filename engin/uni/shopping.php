<?php

register('shop',function () {
    $data = [];
    foreach ( listing('list/product',null) as $entry ) {
        $item = [
            'name' => $entry,
            'path' => realpath('list/product/'.$entry),
        ];

        $item['mime'] = strtolower(pathinfo($item['path'], PATHINFO_EXTENSION));
        
        $item['icon'] = fa_icon($item['mime']);

        $item['text'] = substr($item['name'],0,strlen($item['name'])-(strlen($item['mime'])+1));

        $item['logo'] = "/shop/{$item['text']}.png";
        
        switch ($item['mime']) {
            case 'jsonld':
                $item['data'] = json_decode(file_get_contents($item['path']));
                break;
        }

        $data[] = $item;
    }
    return $data;
},'json');

