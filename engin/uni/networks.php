<?php

register('cidr',function () {
    $data = [];
    foreach ( listing('cidr',null) as $entry ) {
        $item = [
            'name' => $entry,
            'path' => realpath('cidr/'.$entry),
        ];

        $item['text'] = explode("\n",file_get_contents($item['path']));
        
        foreach ($item['text'] as $line) {
            $line = explode('-',$line,2);

            if (sizeof($line)==2) {
                $data[] = [
                    'alias' => $item['name'],
                    'begin' => $line[0],
                    'finis' => str_replace("\n",'',$line[1]),
                ];
            }
        }
   }
    return $data;
},'json');

//##########################################################################################

register('ipv4',function () {
    $data = [];
    foreach ( listing('cidr',null) as $entry ) {
        $rpath = realpath('cidr/'.$entry);

        $source = explode("\n",file_get_contents($rpath));
        
        foreach ($source as $line) {
            $line = explode('-',$line,2);

            if (sizeof($line)==2) {
                $item = [
                    'alias' => $item['name'],
                    'begin' => $line[0],
                    'finis' => str_replace("\n",'',$line[1]),
                ];
                
                $data[] = $item;
            }
        }
   }
    return $data;
},'json');

/******************************************************************************/


