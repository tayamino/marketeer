<?php

register('code',function () {
    $data = [];
    foreach (['core','plug'] as $nrw) {
        if (isset($_GET[$nrw])) {
            foreach ($_GET[$nrw] as $key) {
                $maps = [
                    'core' => "php",
                    'plug' => "uni",
                ];

                $html = file_get_contents("../asset/{$maps[$nrw]}/{$key}.php");
                
                $html = str_replace("<?php\n\n",'',$html);
                
                $data[] = $html;
            }
        }
    }
    return $data;
},'json');

