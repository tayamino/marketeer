<?php

register('back',function () {
    if (isset($_POST['name']) and isset($_POST['link'])) {
        $resp = [];
        $numb = 0;
        foreach (explode("\n",$_POST['link']) as $entry) {
            if (strlen($entry)) {
                file_get_contents("back/{$_POST['name']}-{$numb}",$entry);
            }
        }
    }
    $data = [];
    foreach ( listing('back',null) as $entry ) {
        $item = [
            'uuid' => $entry,
            'link' => str_replace("\n",'',file_get_contents("back/{$entry}")),
        ];

        $link = parse_url($item['link']);
        
        foreach ([
            'type' => "scheme",
            'host' => "host",
            'port' => "port",
            'user' => "user",
            'pass' => "pass",
            'path' => "path",
            'args' => "query",
            'hash' => "fragment",
        ] as $k => $v) {
            $item[$k] = $link[$v];
        }
        
        //print_r($link);die(1);

        $item['icon'] = fa_icon($item['type']);
        
        $data[] = $item;
    }
    return $data;
},'json');

