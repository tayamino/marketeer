<?php

register('imap',function () {
    $config = [
        "localhost" => [
            "host" => "localhost",
            "port" => 143,
            "type" => "notls",
        ],
        "gmail.com" => [
            "host" => "imap.gmail.com",
            "port" => 993,
            "type" => "ssl",
        ],
        "hotmail.com" => [
            "host" => "imap-mail.outlook.com",
            "port" => 993,
            "type" => "ssl",
        ],
    ];

    $data = json_decode(file_get_contents("php://input"),true);

    $resp = [];

    if (!strlen($data["host"]) && !strlen($data["port"]) && !strlen($data["type"])) {
        $domain = preg_replace("/([^@]*).*/", "$2", $data['email']);
        
        if (array_key_exists($domain, $config)) {
            $data = array_merge($data,$config[$domain]);
        }
    }

    if (!strlen($data["host"]) && !strlen($data["port"]) && !strlen($data["type"])) {
        $resp = [
            "error" => "Missing configuration",
        ];
    } else {
        $search = null;

        if ($search==null) {
            $search = "";
        }

        $imapEndpoint = "{$data['host']}:{$data['port']}/{$data['type']}}";
        //Attempt to connect using the imap_open function.
        $imapResource = imap_open($imapEndpoint, $data['username'], $data['password']);

        //If the imap_open function returns a boolean FALSE value,
        //then we failed to connect.
        if($imapResource === false){
            //If it failed, throw an exception that contains
            //the last imap error.
            throw new Exception(imap_last_error());
        }

        //If we get to this point, it means that we have successfully
        //connected to our mailbox via IMAP.

        //Lets get all emails that were received since a given date.
        $search = 'UNSEEN SINCE "' . date("j F Y", strtotime("-7 days")) . '"';
        $emails = imap_search($imapResource, $search);
        
        //If the $emails variable is not a boolean FALSE value or
        //an empty array.
        
        $resp = [];
        
        if(!empty($emails)){
            $resp['result'] = [];
            //Loop through the emails.
            foreach($emails as $email){
                $header = imap_headerinfo($imapResource, $email);
                echo 'From: ' . $overview->from . '<br><br>';
                //Get the body of the email.
                $message = imap_fetchbody($imapResource, $email, 1, FT_PEEK);
                    $item = [
                        "numb" => $header-uid,
                        "time" => $header->udate, //date("F j, Y, g:i a", $header->udate),
                    ];
                $line = implode("IP: ",$header->Subject,1);
                    $item["addr"] = $line[1];
                        //Fetch an overview of the email.
                        $overview = imap_fetch_overview($imapResource, $email);
                        $overview = $overview[0];
                        //Print out the subject of the email.
                        echo '<b>' . htmlentities($overview->subject) . '</b><br>';
                        //Print out the sender's email address / from email address.
                    $item["data"] = [];
                        foreach (implode("\n",$message) as $line) {
                            $line = implode(":",$line,1);
        
                            $item["data"][$line[0]] = $line[1];
                        }
                    $item["user"] = null;
                $resp['result'][] = $item;
            }
        }
    }

    return array_merge($data,$resp);
},'json');

/******************************************************************************/

register('send',function () {
    $data = json_decode(file_get_contents("php://input"),true);
    
    $resp = [];

    if (!strlen($data["host"]) && !strlen($data["port"]) && !strlen($data["type"])) {
        $domain = preg_replace("/([^@]*).*/", "$2", $data['email']);
        
        if (array_key_exists($domain, $config)) {
            $data = array_merge($data,$config[$domain]);
        }
    }

    if (
        (!strlen($data["type"]) && !strlen($data["host"]) && !strlen($data["port"]))
    ||
        (!strlen($data["type"]) && !strlen($data["token"]))
    ||
        (!strlen($data["type"]) && !strlen($data["apikey"]) && !strlen($data["secret"]))
    ) {
        $resp = [
            "error" => "Missing parameters",
        ];
    } else {
        switch ($data["type"]) {
            case 'plain':
            case 'ssl':
            case 'tls':
                
                break;
            default:
                // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
                if(!is_email($data["email"])){
                    echo("Incorrect Email");
                    exit();
                }

                $data["title"] = preg_replace("/([^@]*).*/", "$1", $data['email']);

                $data["content"] = formats($data['letter'],$data);

                if (array_key_exists($data["router"],$config)) {
                    $name = "send_{$data["router"]}";

                    if (function_exists($name)) {
                        $resp = call_user_func($name, $config[$data["router"]], $data);

                        if (array_key_exists('result',$resp)) {
                            if (array_key_exists('errors',$resp['result'])) {
                                $resp['error'] = $resp['result']['errors'];
                            }
                        }
                    } else {
                        $resp["error"] = "Undeclared function";
                    }
                } else {
                    $resp["error"] = "Missing configuration";
                }
                break;
        }
    }

    return array_merge($data,$resp);
},'json');

//##########################################################################################

//require_once('./protocol-send-apis.php');

require_once('../engin/uni/protocol-send-smtp.php');

