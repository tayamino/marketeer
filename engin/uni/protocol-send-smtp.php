<?php

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function

/*
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
*/

//Load Composer's autoloader
//require 'vendor/autoload.php';

/******************************************************************************/

register('smtp',function () {
    $data = json_decode(file_get_contents("php://input"),true);

    $resp = [
        'form' => $data,
    ];

    try {
        //Create an instance; passing `true` enables exceptions
        $mail = new PHPMailer(true);

        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = $data['server']['host'];                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = $data['server']['username'];                     //SMTP username
        $mail->Password   = $data['server']['password'];                               //SMTP password
        $mail->Port       = $data['server']['port'];                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        switch ($data['server']['type']) {
            case 'ssl':
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                break;
            case 'tls':
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
                break;
            default:
                //$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                break;
        }

        $mail->setFrom($data['server']['from_email']);

        $mail->addAddress($data['address']);

        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = $data['subject'];
        $mail->Body    = $data['content'];
        $mail->AltBody = $data['message'];

        $mail->send();

        $resp['code'] = 'success';
        //echo 'Message has been sent';
    } catch (Exception $e) {
        $resp['code'] = 'error';

        $resp['text'] = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

    return $resp;
},'json');

//##########################################################################################
