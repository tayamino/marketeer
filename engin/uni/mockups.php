<?php

register('mock',function () {
    $data = [];
    foreach ( listing('mock','folder') as $entry ) {
        $item = [
            'name' => $entry,
            'logo' => null,
            //'page' => scandir('mock'.'/'.$entry),
            'page' => [],
        ];
        foreach ( scandir('mock/'.$entry) as $page ) {
            if (substr($page,-5)=='.html') {
                $item['page'][] = substr($page,0,-5);
            }
        }
        foreach (['ico','png','svg'] as $ext) {
            if ($item['logo']==null) {
                if (is_file("mock/{$entry}/favicon.{$ext}")) {
                    $item['logo'] = "http://{$_SERVER['HTTP_HOST']}/mock/{$entry}/favicon.{$ext}";
                }
            }
        }
        $data[] = $item;
    }
    return $data;
},'json');

/******************************************************************************/

register('wget',function () {
    $ch = curl_init();
    curl_setopt_array($ch, [
        CURLOPT_URL => $_GET['link'],
        CURLOPT_RETURNTRANSFER => true
    ]);
    $result = curl_exec($ch);
    curl_close($ch);
    header('Content-Type: text/plain');
    return $result;
    /*
    echo "name : {$_GET['name']}<br>";
    echo "page : {$_GET['page']}<br>";
    echo "link : {$_GET['link']}<br>";
    die(1);
    //*/
},'json');

